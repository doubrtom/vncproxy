/* 
 * File:   CConnection.h
 * Author: tomas
 *
 * Created on 18. únor 2014, 12:08
 */

#ifndef CCONNECTION_H_23456787654
#define	CCONNECTION_H_23456787654

namespace doubrtom {
	
	/**
	 * Class for control communication of vncproxy-server and vncproxy-control
	 */
	class CConnection {
	public:
		virtual ~CConnection() {}
		
		enum Response {
			RESPONSE_OK, RESPONSE_FAIL, RESPONSE_UNKNOWN_CMD, RESPONSE_ALREADY_SET,
			RESPONSE_INVALID_ARGUMENT, RESPONSE_NOT_CONNECTED
		};
		
		enum Command {
			START_RECORD, STOP_RECORD, SHOW_STATUS, START_SCREEN_CAPTURE,
			STOP_SCREEN_CAPTURE, SET_LOG_FILE, SET_RECORD_FILE, START_PLAYBACK,
			STOP_PLAYBACK, PLAYBACK_SPEED, SET_IMAGE_DIR
		};
		
	protected:
		static const int COMMAND_LENGT = 11;
		
	};
	
}



#endif	/* CCONNECTION_H */

