#include <cstdlib>
#include <errno.h>
#include <error.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>

#include "Communicator.h"
#include "Exception.h"
#include "Communicator.h"
#include "../server/Server.h"

using namespace doubrtom;

Communicator::Communicator(Server & s, const char * callerName) : server(s) {
	int callerNameLength = strlen(callerName);
	this->callerName = new char[callerNameLength + 1];
	strcpy(this->callerName, callerName);
	this->callerName[callerNameLength] = 0;
	
	bufferSize = 500000;
//	bufferSize = 50000; // NE MIN, musi prenest data (forward) pred zpracovanim
	inBuffer = new U8[bufferSize];
	inBufferPointer = inBuffer;
	outBuffer = new U8[bufferSize];
	outBufferPointer = outBuffer;
	inBufferSize = 0;
	outBufferSize = bufferSize;
	
	isBigEndian = (htonl(47) == 47);

	memset(inBuffer, 0, bufferSize);
	memset(outBuffer, 0, bufferSize);
}

Communicator::~Communicator() {
	delete[] callerName;
	delete[] inBuffer;
	delete[] outBuffer;
}

void Communicator::setSockets(int src, int dst) {
	// bofore change sockets, flush unsent data.
	flushBuffer();
	
	srcSocket = src;
	dstSocket = dst;
}

void Communicator::readFromSocket() {
	size_t r = 0;
	FD_ZERO(&masterSet);
	FD_SET(srcSocket, &masterSet);
	
	while (server.isRunning()) {
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		workingSet = masterSet;
		
		selected = select(srcSocket+1, &workingSet, NULL, NULL, &timeout);
		
		if (selected < 0) {
			if (errno == EINTR) throw Exception(Exception::EX_EINTR);
			throw Exception(Exception::EX_DCONN_COMM, "select", strerror(errno), errno);
		}
		if (selected == 0) continue;
		
		if (FD_ISSET(srcSocket, &workingSet)) {
			r = ::recv(srcSocket, inBuffer + inBufferSize, bufferSize - inBufferSize, 0);
			
			if (r < 0) {
				if (errno == EWOULDBLOCK || errno == EAGAIN) continue;
				if (errno == EINTR) throw Exception(Exception::EX_EINTR);
				throw Exception(Exception::EX_DCONN_COMM, "receive", strerror(errno), errno);
			}
			if (r == 0) throw Exception(Exception::EX_COMM_CLOSED_BY_PEER);
			
			inBufferSize += r;
			
			break;
		}
	}
	
	if (!server.isRunning()) throw Exception(Exception::EX_VNCPROXY_CLOSED_BY_USER);
}

void Communicator::sendToSocket() {
	size_t len = bufferSize - outBufferSize;
	if (len == 0) return;
	
	int r = ::send(dstSocket, outBuffer, len, 0);
	
	if (r < 0) {
		if (errno == EINTR) throw Exception(Exception::EX_EINTR);
		throw Exception(Exception::EX_DCONN_COMM, "send", strerror(errno), errno);
	}
	if (r == 0) throw Exception(Exception::EX_COMM_CLOSED_BY_PEER);
	
	outBufferPointer = outBuffer;
	outBufferSize = bufferSize;
}

/* ------------------------------------------- */

void Communicator::read(size_t len) {
	while (inBufferSize < len) loadData();
	inBufferPointer += len;
	inBufferSize -= len;
}

void Communicator::read(void * dst, size_t len) {
	while (inBufferSize == 0) loadData();
	
	U8 * dstPointer = (U8*) dst;
	while (len > 0) {
		if (inBufferSize >= len) {
			memcpy(dstPointer, inBufferPointer, len);
			inBufferPointer += len;
			inBufferSize -= len;
			return;
		} else {
			memcpy(dstPointer, inBufferPointer, inBufferSize);
			len -= inBufferSize;
			dstPointer += inBufferSize;
			inBufferSize = 0;
			while (inBufferSize == 0) loadData();
		}
	}
}

void Communicator::readU8(U8 & dst) {
	read(&dst, 1);
}

void Communicator::readU16(U16 & dst) {
	read(&dst, 2);
	dst = ntohs(dst);
}

void Communicator::readU32(U32 & dst) {
	read(&dst, 4);
	dst = ntohl(dst);
}

void Communicator::readS8(S8 & dst) {
	read(&dst, 1);
}

void Communicator::readS16(S16 & dst) {
	read(&dst, 2);
	dst = ntohs(dst);
}

void Communicator::readS32(S32 & dst) {
	read(&dst, 4);
	dst = ntohl(dst);
}

void Communicator::send(void * src, size_t len) {
	if (outBufferSize == 0) sendToSocket();
	U8 * srcPointer = (U8 *) src;
	
	while (len > 0) {
		if (len <= outBufferSize) {
			memcpy(outBufferPointer, srcPointer, len);
			outBufferPointer += len;
			outBufferSize -= len;
			return;
		} else {
			memcpy(outBufferPointer, srcPointer, outBufferSize);
			len -= outBufferSize;
			srcPointer += outBufferSize;
			outBufferSize = 0;
			sendToSocket();
		}
	}
}

void Communicator::send(size_t len) {
	if (outBufferSize == 0) sendToSocket();
	
	while (len > 0) {
		if (len <= outBufferSize) {
			outBufferPointer += len;
			outBufferSize -= len;
			return;
		} else {
			len -= outBufferSize;
			outBufferSize = 0;
			sendToSocket();
		}
	}
}

void Communicator::sendU8(U8 src) {
	send(&src, 1);
}

void Communicator::sendU16(U16 src) {
	U16 tmp = htons(src);
	send(&tmp, 2);
}

void Communicator::sendU32(U32 src) {
	U32 tmp = htonl(src);
	send(&tmp, 4);
}

void Communicator::sendS8(S8 src) {
	send(&src, 1);
}

void Communicator::sendS16(S16 src) {
	S16 tmp = htons(src);
	send(&tmp, 2);
}

void Communicator::sendS32(S32 src) {
	S32 tmp = htonl(src);
	send(&tmp, 4);
}

void Communicator::forward(void * mem, size_t len) {
	read(mem, len);
	send(mem, len);
}

void Communicator::forward(size_t len) {
	while (inBufferSize < len) loadData();
	if (outBufferSize < len) flushBuffer();
	
	memcpy(outBufferPointer, inBufferPointer, len);
	outBufferPointer += len;
	inBufferPointer += len;
	inBufferSize -= len;
	outBufferSize -= len;
}

void Communicator::forwardU8(U8 & item) {
	readU8(item);
	sendU8(item);
}

void Communicator::forwardU16(U16 & item) {
	readU16(item);
	sendU16(item);
}

void Communicator::forwardU32(U32 & item) {
	readU32(item);
	sendU32(item);
}

void Communicator::forwardS8(S8 & item) {
	readS8(item);
	sendS8(item);
}

void Communicator::forwardS16(S16 & item) {
	readS16(item);
	sendS16(item);
}

void Communicator::forwardS32(S32 & item) {
	readS32(item);
	sendS32(item);
}

//void Communicator::forwardPixel(Pixel & pixel) {
//	switch (server.getVncServerInfo().pixelFormat.bitsPerPixel) {
//		case 8: {
//			U8 p;
//			forwardU8(p);
//			pixel = p;
//		} break;
//		case 16: {
//			U16 p;
//			forwardU16(p);
//			pixel = p;
//		} break;
//		case 32: {
//			U32 p;
//			forwardU32(p);
//			pixel = p;
//		} break;
//		default:
//			throw Exception(
//					Exception::EX_INVALID_VALUE_BPP, 
//					server.getVncServerInfo().pixelFormat.bitsPerPixel
//			);
//	}
//}
void Communicator::forwardPixel(Pixel & pixel) {
	switch (server.getVncServerInfo().pixelFormat.bitsPerPixel) {
		case 8: {
			U8 p;
			forwardU8(p);
			pixel = p;
		} break;
		case 16: {
			U16 p;
			
			read(&p, 2);
			send(&p, 2);
			if (isBigEndian != server.getVncServerInfo().pixelFormat.bigEndianFlag) p = __bswap_16(p);
				
			pixel = p;
		} break;
		case 32: {
			U32 p;
			
			read(&p, 4);
			send(&p, 4);
			if (isBigEndian != server.getVncServerInfo().pixelFormat.bigEndianFlag) p = __bswap_32(p);
			
			pixel = p;
		} break;
		default:
			throw Exception(
					Exception::EX_INVALID_VALUE_BPP, 
					server.getVncServerInfo().pixelFormat.bitsPerPixel
			);
	}
}

//U8 Communicator::getU8() {
//	U8 tmp = *lastGet;
//	lastGet++;
//	return tmp;
//}
//
//U16 Communicator::getU16() {
//	U16 tmp = *((U16 *) lastGet);
//	lastGet += 2;
//	return tmp;
//}
//
//U32 Communicator::getU32() {
//	U32 tmp = *((U32 *) lastGet);
//	lastGet += 4;
//	return tmp;
//}
//
//S8 Communicator::getS8() {
//	return (S8) getU8();
//}
//
//S16 Communicator::getS16() {
//	return (S16) getS16();
//}
//
//S32 Communicator::getS32() {
//	return (S32) getS32();
//}

void Communicator::skip(size_t bytes) {
	// load data
	read(bytes);
	
	// send data
	send(bytes);
}

void Communicator::flushBuffer() {
	if (outBufferSize == bufferSize) return;
	
	sendToSocket();
}

void Communicator::loadData() {
	if (inBufferSize > 0) memmove(inBuffer, inBufferPointer, inBufferSize);
	inBufferPointer = inBuffer;
	readFromSocket();
}

bool Communicator::isEmptyIn() {
	return (inBufferSize == 0);
}

bool Communicator::isEmptyOut() {
	return (outBufferSize == bufferSize);
}