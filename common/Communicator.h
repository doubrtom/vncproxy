/* 
 * File:   Communicator.h
 * Author: tomas
 *
 * Created on 24. únor 2014, 22:16
 */

#ifndef COMMUNICATOR_H_87654567
#define	COMMUNICATOR_H_87654567

#include <cstdlib>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>

#include "types.h"

namespace doubrtom {
	
	class Server;
	
	/**
	 * Communicator for VNC server and client.
	 */
	class Communicator {
	public:
		/**
		 * Constructor.
     * @param s	Instance of Server.
     * @param callerName	Name of caller for exception log.
     */
		Communicator(Server & s, const char * callerName = NULL);
		virtual ~Communicator();
		
//		void send(int & socket, void * buffer, size_t len, int flags = 0);
//		void recv(int & socket, void * buffer, size_t len, int flags = 0);
		
		/**
		 * Set sockets of VNC client and VNC server.
     * @param src	Socket for reading.
     * @param dst	Socket for writing.
     */
		void setSockets(int src, int dst);
		/**
		 * Read data from src socket into communicator in-buffer.
     */
		void readFromSocket();
		/**
		 * Send data to dst socket from communicator out-buffer.
     */
		void sendToSocket();
		
		/**
		 * Copy data from in-buffer.
     * @param len	Number of bytes for reading.
     */
		void read(size_t len);
		/**
		 * Copy data from in-buffer into dst buffer.
     * @param dst	Destination buffer for reading.
     * @param len	Number of bytes for reading.
     */
		void read(void * dst, size_t len);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readU8(U8 & dst);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readU16(U16 & dst);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readU32(U32 & dst);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readS8(S8 & dst);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readS16(S16 & dst);
		/**
		 * Copy data from in-buffer into dst.
     * @param dst	Destination variable for reading.
     */
		void readS32(S32 & dst);

		/**
		 * Copy data to out-buffer.
     * @param len	Number of bytes for sending.
     */
		void send(size_t len);
		/**
		 * Copy data from src buffer into out-buffer.
     * @param src	Buffer for sending.
     * @param len	Number of bytes in src buffer.
     */
		void send(void * src, size_t len);
		/**
		 * Copy src to out-buffer.
     * @param src	Data for sending.
     */
		void sendU8(U8 src);
		/**
		 * Copy src to out-buffer
     * @param src	Data for sending.
     */
		void sendU16(U16 src);
		/**
		 * Copy src to out-buffer
     * @param src	Data for sending.
     */
		void sendU32(U32 src);
		/**
		 * Copy src to out-buffer
     * @param src	Data for sending.
     */
		void sendS8(S8 src);
		/**
		 * Copy src to out-buffer
     * @param src	Data for sending.
     */
		void sendS16(S16 src);
		/**
		 * Copy src to out-buffer
     * @param src	Data for sending.
     */
		void sendS32(S32 src);

		/**
		 * Copy data from in-buffer into mem buffer and out-buffer.
     * @param mem	Buffer for copying data.
     * @param len	Number of bytes for copying.
     */
		void forward(void * mem, size_t len);
		/**
		 * Copy data from in-buffer into out-buffer.
     * @param len	Number of bytes for copying.
     */
		void forward(size_t len);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardU8(U8 & item);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardU16(U16 & item);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardU32(U32 & item);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardS8(S8 & item);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardS16(S16 & item);
		/**
		 * Copy data from in-buffer into out-buffer and item.
     * @param item	Destination variable for reading.
     */
		void forwardS32(S32 & item);
		/**
		 * Copy data from in-buffer into out-buffer and pixel. Copy number of bytes according to pixel format.
     * @param item	Destination variable for reading.
     */
		void forwardPixel(Pixel & pixel);
		
		/**
		 * Skip number of bytes in in-buffer.
     * @param bytes	Number of bytes for skipping.
     */
		void skip(size_t bytes);
		/**
		 * Send all data from out-buffer into dst socket.
     */
		void flushBuffer();
		/**
		 * Load data from src socket into in-buffer.
     */
		void loadData();
		/**
		 * Check if in-buffer is empty.
     * @return True if in-buffer is empty else false.
     */
		bool isEmptyIn();
		/**
		 * Check if out-buffer is empty.
     * @return True if out-buffer is empty else false.
     */
		bool isEmptyOut();
		
		
	protected:
		Server & server;
		char * callerName;
		bool isBigEndian;
		
		int srcSocket;
		int dstSocket;
		
		U8 * inBuffer;
		U8 * outBuffer;
		U8 * inBufferPointer;
		U8 * outBufferPointer;
		size_t bufferSize;
		size_t inBufferSize;
		size_t outBufferSize;
		
		// for select()
		fd_set masterSet, workingSet;
		int maxSocket;
		int selected;
		timeval timeout;
	};
	
}



#endif	/* COMMUNICATOR_H */

