#include <string.h>

#include "Cursor.h"
#include "../server/Server.h"
#include "../screen/convertor/ConvertorTrueColour.h"
#include "../screen/convertor/ConvertorBase.h"
#include "Exception.h"

using namespace doubrtom;

Cursor::Cursor(U8 * buffer, U8 * bitmask, U16 width, U16 height, U16 hotspotX, U16 hotspotY, Server & server) : server(server) {
	this->buffer = NULL;
	this->bitmask = NULL;
	dimensions.x = hotspotX;
	dimensions.y = hotspotY;
	dimensions.width = width;
	dimensions.height = height;
	
	setBuffer(buffer, width, height, server);
	U16 bitmaskSize = ((width+7)/8) * height;
	setBitmask(bitmask, bitmaskSize);
}

Cursor::Cursor(U8 * buffer, U8 * bitmask, Rect & rect, Server & server) : server(server) {
	this->buffer = NULL;
	this->bitmask = NULL;
	dimensions = rect;
	
	setBuffer(buffer, rect.width, rect.height, server);
	U16 bitmaskSize = ((rect.width+7)/8) * rect.height;
	setBitmask(bitmask, bitmaskSize);
}

Cursor::Cursor(const Cursor & c) : server(c.server) {
	dimensions = c.dimensions;
	position = c.position;
	
	buffer = new U8[dimensions.width * dimensions.height * 3];
	memcpy(buffer, c.buffer, dimensions.width * dimensions.height * 3);

	U16 bitmaskSize = ((dimensions.width+7)/8) * dimensions.height;
	bitmask = new U8[bitmaskSize];
	memcpy(bitmask, c.bitmask, bitmaskSize);
}

Cursor::~Cursor() {
	delete[] buffer;
	delete[] bitmask;
}

Cursor & Cursor::operator =(const Cursor& c) {
	if (this == &c) return *this;
	
	position = c.position;
	dimensions = c.dimensions;
	
	delete[] buffer;
	buffer = new U8[dimensions.width * dimensions.height * 3];
	memcpy(buffer, c.buffer, dimensions.width * dimensions.height * 3);
	
	U16 bitmaskSize = ((dimensions.width+7)/8) * dimensions.height;
	setBitmask(c.bitmask, bitmaskSize);
	
	return *this;
}


void Cursor::setBuffer(U8 * buffer, U16 width, U16 height, Server & server) {
	if (this->buffer != NULL) delete[] this->buffer;
	
	this->buffer = new U8[width * height * 3];
	dimensions.width = width;
	dimensions.height = height;
	
	ConvertorTrueColour con(this->buffer, width, height, server.getVncServerInfo().pixelFormat);
	con.setRect(0, 0, width, height);
	con.copyRect((void *) buffer);
}

void Cursor::setBitmask(U8 * bitmask, U16 bytes) {
	if (this->bitmask != NULL) delete[] this->bitmask;
	
	this->bitmask = new U8[bytes];
	memcpy(this->bitmask, bitmask, bytes);
}

void Cursor::setPosition(U16 x, U16 y) {
	position.x = x;
	position.y = y;
}

void Cursor::setDimensions(U16 width, U16 height) {
	dimensions.width = width;
	dimensions.height = height;
}

void Cursor::setHotspot(U16 x, U16 y) {
	dimensions.x = x;
	dimensions.y = y;
}