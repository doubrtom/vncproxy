/* 
 * File:   Cursor.h
 * Author: tomas
 *
 * Created on 13. duben 2014, 11:01
 */

#ifndef CURSOR_H_87654567
#define	CURSOR_H_87654567

#include "types.h"
#include "Rect.h"
#include "Point.h"

namespace doubrtom {
	
	class Server;
	
	/**
	 * Represend position and image data for cursor.
	 */
	class Cursor {
	public:
		/**
		 * Constructor.
     * @param buffer	Buffer with image data.
     * @param bitmask	Buffer with bitmask data.
     * @param width	Width of cursor buffer.
     * @param height	Height of cursor buffer.
     * @param hotspotX	X coordinate of cursor hotspot.
     * @param hotspotY	Y coordinate of cursor hotspot.
     * @param server	Instance of server.
     */
		Cursor(U8 * buffer, U8 * bitmask, U16 width, U16 height, U16 hotspotX, U16 hotspotY, Server & server);
		/**
		 * Constructor.
     * @param buffer	Buffer with image data.
     * @param bitmask	Buffer with bitmask data.
     * @param rect		Rect with width and height of cursor buffer, X and Y of cursor hotspot.
     * @param server	Instance of server.
     */
		Cursor(U8 * buffer, U8 * bitmask, Rect & rect, Server & server);
		Cursor(const Cursor & c);
		~Cursor();
		
		Cursor & operator=(const Cursor & c);
		
		/**
		 * Take data from cursor pseudo encoding in VNC format and save it in RGB888 format for libpng.
		 * 
     * @param buffer		Buffer with VNC cursor pseudo encoding data
     * @param width			Width of cursor data
     * @param height		Height of cursor data
     * @param server		Reference for Server class.
     */
		void setBuffer(U8 * buffer, U16 width, U16 height, Server & server);
		
		/**
		 * Save bitmask for cursor.
		 * 
     * @param bitmask		Buffer with bitmask data.
     * @param bytes			Size of buffer in bytes, calculate: floor((width+7)/8) * height
     */
		void setBitmask(U8 * bitmask, U16 bytes);
		
		/**
		 * Set new position of cursor inside screen in abslute coordinates.
		 * 
     * @param x		X coordinate of new cursor position
     * @param y		Y coordinate of new cursof position
     */
		void setPosition(U16 x, U16 y);
		
		/**
		 * Set cursor dimensions: width and height
		 * 
     * @param width		new width of cursor
     * @param height	new height of cursor
     */
		void setDimensions(U16 width, U16 height);
		
		/**
		 * Set cursor hotspot in buffer.
     * @param x	X coordinate of hotspot.
     * @param y	Y coordinate of hotspot.
     */
		void setHotspot(U16 x, U16 y);
		
		
		U8 * buffer;
		U8 * bitmask;
		Rect dimensions; // width, height -> cursor; x,y -> hotspot of cursor
		Point position;
		
		Server & server;
	};
	
}

#endif	/* CURSOR_H_87654567 */

