#include <cstdlib>
#include <string.h>
#include <stdio.h>
#include <cstdarg>

#include "Exception.h"

using namespace doubrtom;

const char * const EXCEPTION_TEXT[] = {
	"Unable to create data connection: %s (%d)",
	"Error during communication (%s): %s (%d)",
	"Vncproxy closed by user.",
	"Communication closed by peer.",
	"Unable open file: %s.",
	"Invalid XML attribute '%s' in node '%s'.",
	"Invalid value of bits per pixel: %d.",
	"Error in zlib decoder: %s.",
	"Screen saver can't open file for writing: %s.",
	"Interrupted by SIGTERM.",
	"%s: Unable to create new thread.",
        "Error during VNC hand shaking: %s."
};

Exception::Exception() {
    msg[0] = 0;
		code = EX_CODE_NOT_SET;
}

Exception::Exception(const char * str, ...) {
	code = EX_CODE_NOT_SET;
	
	va_list args;
	va_start(args, str);
	vsprintf(msg, str, args);
	va_end(args);
}

Exception::Exception(EXCEPTION_CODE code, ...) {
	this->code = code;
	
	va_list args;
	va_start(args, code);
	vsprintf(msg, EXCEPTION_TEXT[code], args);
	va_end(args);
}

const char * Exception::getMessage() const {
    return msg;
}

Exception::EXCEPTION_CODE Exception::getCode() const {
	return code;
}