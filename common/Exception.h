/* 
 * File:   Exception.h
 * Author: tomas
 *
 * Created on 5. únor 2014, 20:42
 */

#ifndef EXCEPTION_H_5678987678
#define	EXCEPTION_H_5678987678

namespace doubrtom {

	/**
	 *	Class for exception in application.
	 */
	class Exception {
	public:
		
		enum EXCEPTION_CODE {
			EX_DCONN_CONN, 
			EX_DCONN_COMM,
			EX_VNCPROXY_CLOSED_BY_USER,
			EX_COMM_CLOSED_BY_PEER,
			EX_UNABLE_OPEN_FILE,
			EX_BAD_XML_ATTRIBUTE,
			EX_INVALID_VALUE_BPP,
			EX_ZLIB_DECODER,
			EX_SCREEN_SAVER_UNABLE_CREATE_FILE,
			EX_EINTR,
			EX_CREATE_THREAD,
                        EX_VNC_HAND_SHAKING,
			
			EX_CODE_NOT_SET
		};
		
		Exception();
		/**
		 * Create Exception by code with predefined text.
     * @param code	Code of exception
     * @param ...		Parametrs for printf
     */
		Exception(EXCEPTION_CODE code, ...);
		/**
		 * Create Exception with str message.
     * @param str	Exception message
     * @param ...	Parametrs for printf
     */
		Exception(const char * str, ...);
		const char * getMessage() const;
		EXCEPTION_CODE getCode() const;

	protected:
		char msg[512];
		EXCEPTION_CODE code;
	};

}

#endif	/* EXCEPTION_H */

