#define XK_LATIN1 true
#define XK_MISCELLANY true
#include <X11/keysymdef.h>

#include "KeyTranslator.h"

using namespace doubrtom;

const char * KeyTranslator::translate(U32 keyCode) {
	// latin characters
	if (keyCode >= XK_space && keyCode <= XK_asciitilde) {
		return LATIN_CHARS[keyCode - XK_space];
	}
	if (keyCode >= XK_Shift_L && keyCode <= XK_Hyper_R) {
		return MODIFIERS[keyCode - XK_Shift_L];
	}
	
	return "";
}

U32 KeyTranslator::translate(const char ch) {
	U32 code = (U32) ch;
	code -= (int) ' ';
	code = XK_space + code;
	
	return code;
}

// array for translate key code into character and XML entities
const char * KeyTranslator::LATIN_CHARS[] = {
	" ", "!", "&quot;", "#", "$", "%", "&amp;", "&apos;", "(", ")", "*", "+", ",", "-", ".",
	"/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "&lt;", "=", 
	"&gt;", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", 
	"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[",
	"\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
	"k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
	"z", "{", "|", "}", "~"
};

const char * KeyTranslator::MODIFIERS[] = {
	"Left shift", "Right shift", "Left control", "Right control",	"Caps lock",
	"Shift lock", "Left meta", "Right meta", "Left alt", "Right alt", 
	"Left super", "Right super", "Left hyper", "Right hyper"
};