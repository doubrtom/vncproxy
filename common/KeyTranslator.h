/* 
 * File:   KeyTranslator.h
 * Author: tomas
 *
 * Created on 15. březen 2014, 15:02
 */

#ifndef KEYTRANSLATOR_H_876545678
#define	KEYTRANSLATOR_H_876545678

#include "types.h"

namespace doubrtom {
	
	/**
	 * Class for translation key code into string with key name and vice versa.
	 */
	class KeyTranslator {
	public:
		/**
		 * Translate key code into key name.
     * @param keyCode	Key code for translate.
     * @return	Return key name or empty string.
     */
		static const char * translate(U32 keyCode);
		/**
		 * Translate key name into key code. Use only for latin characters.
     * @param ch	Key name for translate.
     * @return Return key code.
     */
		static U32 translate(const char ch);
		
	protected:
		static const char * LATIN_CHARS[];
		static const char * MODIFIERS[];
	};
	
}

#endif	/* KEYTRANSLATOR_H_876545678 */

