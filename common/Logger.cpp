#include <cstdio>
#include <cstdarg>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <stdlib.h>

#include "Logger.h"
#include "Exception.h"

using namespace doubrtom;
using namespace std;

Logger::Logger() {
	pthread_mutex_init(&mutex, NULL);
}

Logger::~Logger() {
	if (file.is_open()) file.close();
  pthread_mutex_destroy(&mutex);
}

void Logger::init(const char * host, int display) {
  const char * homeDir = getenv("HOME");
	if (homeDir == NULL) throw Exception("Missing environment variable HOME.");
  
  char filePath[500];
  sprintf(filePath, "%s/.vncproxy/%s:%d.log", homeDir, host, display);
  
  file.open(filePath, fstream::out);
  
  if (!file.is_open()) throw Exception("Cannot create log file: %s.", filePath);
}

void Logger::log(const char * str, ...) {
	pthread_mutex_lock(&mutex);
	
	va_list args;
	va_start(args, str);
	vsprintf(buffer, str, args);
	va_end(args);

	writeOut();
	
	pthread_mutex_unlock(&mutex);
}

void Logger::writeOut() {
  file << getDateTime() << ": " << buffer << endl;
}

// code from stackoverflow
// http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
const std::string Logger::getDateTime() {
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof (buf), "%Y-%m-%d %X", &tstruct);

	return buf;
}