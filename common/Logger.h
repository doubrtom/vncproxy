/* 
 * File:   Logger.h
 * Author: tomas
 *
 * Created on 8. únor 2014, 18:03
 */

#ifndef LOGGER_H_645678918276
#define	LOGGER_H_645678918276

#include <fstream>
#include <string>
#include <pthread.h>

namespace doubrtom {

  class Logger {
  public:
    Logger();
    virtual ~Logger();

		/**
		 * Init logger. Create file for logging.
     * @param host	Host name of VNC server.
     * @param display	Display number of proxy server.
     */
    void init(const char * host, int display);

		/**
		 * Create log entry in log file.
     * @param str	String with log in printf format.
     * @param ...	Parameters for printf.
     */
    void log(const char * str, ...);

  protected:
    char buffer[1000];
    std::fstream file;

    pthread_mutex_t mutex;
    
    void writeOut(); // print out buffer
    const std::string getDateTime();
  };

}

#endif	/* LOGGER_H */

