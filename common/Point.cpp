#include "Point.h"

using namespace doubrtom;

Point::Point(int x, int y) {
	set(x, y);
}

Point::Point() {
	set(0, 0);
}


void Point::move(int x, int y) {
	this->x += x;
	this->y += y;
}

void Point::set(int x, int y) {
	this->x = x;
	this->y = y;
}

void Point::set(const Point & p) {
	*this = p;
}
