/* 
 * File:   Point.h
 * Author: tomas
 *
 * Created on 16. duben 2014, 20:05
 */

#ifndef POINT_H_8765456010102929383
#define	POINT_H_8765456010102929383

namespace doubrtom {

	/**
	 * Class represent 2D point.
   */
	class Point {
	public:
		/**
		 * Constructor
     * @param x Coordinate of point
     * @param y	Coordinate of point
     */
		Point(int x, int y);
		/**
		 * Constructor. Create point in position [0,0].
     */
		Point();
		
		/**
		 * Move point by 2D vector.
     * @param x Coordinate of vector.
     * @param y	Coordinate of vector.
     */
		void move(int x, int y);
		/**
		 * Set new position of point.
     * @param x Coordinate of point.
     * @param y	Coordinate of point.
     */
		void set(int x, int y);
		/**
		 * Set new position of point
     * @param p	Point with new position.
     */
		void set(const Point & p);
		
		int x;
		int y;
	};
	
}

#endif	/* POINT_H_8765456010102929383 */

