#include "Rect.h"

using namespace doubrtom;

Rect::Rect() {
	set(0, 0, 0, 0);
}

Rect::Rect(int x, int y, int width, int height) {
	set(x, y, width, height);
}

void Rect::set(int x, int y, int width, int height) {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

void Rect::set(const Rect & r) {
	*this = r;
}

void Rect::move(int x, int y) {
	this->x += x;
	this->y += y;
}
