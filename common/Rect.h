/* 
 * File:   Rect.h
 * Author: tomas
 *
 * Created on 31. březen 2014, 23:18
 */

#ifndef RECT_H_8765678
#define	RECT_H_8765678

namespace doubrtom {
	
	/**
	 * Class represent rectangle.
   */
	class Rect {
	public:
		/**
		 * Create rectangle with width and height equal 0 at position [0,0].
     */
		Rect();
		/**
		 * Create rectangle with width and height at position [x,y].
     * @param x	Coordinate of rectangle.
     * @param y	Coordinate of rectangle.
     * @param width	Dimension of rectangle.
     * @param height	Dimension of rectangle.
     */
		Rect(int x, int y, int width, int height);
		
		/**
		 * Set new position and dimensions of rectangle.
     * @param x	Coordinate of rectangle.
     * @param y	Coordinate of rectangle.
     * @param width	Dimension of rectangle.
     * @param height	Dimension of rectangle.
     */
		void set(int x, int y, int width, int height);
		/**
		 * Set new position and dimensions of rectangle.
     * @param r	Rectangle with new data.
     */
		void set(const Rect & r);
		/**
		 * Move rectangle by 2D vector.
     * @param x	Coordinate of 2D vector.
     * @param y	Coordinate of 2D vector.
     */
		void move(int x, int y);
		
		int x;
		int y;
		int width;
		int height;
	};
	
}

#endif	/* RECT_H_8765678 */

