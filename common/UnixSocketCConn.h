/* 
 * File:   UnixSocketCConn.h
 * Author: tomas
 *
 * Created on 18. únor 2014, 18:20
 */

#ifndef UNIXSOCKETCCONN_H_8765456
#define	UNIXSOCKETCCONN_H_8765456

#include <sys/socket.h>
#include <sys/un.h>

#include "CConnection.h"
#include "types.h"

namespace doubrtom {

	/**
	 * Class representing proxy control communication by Unix socket.
	 */
	class UnixSocketCConn : public CConnection {
	public:
		
		
	protected:
		U16 commandHeader[2];
		char arg[512];
		
		char * socketPath;
		
		sockaddr_un serverAddr;
		int serverSocket;
		
	};
	
}


#endif	/* UNIXSOCKETCCONN_H */

