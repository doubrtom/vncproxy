#include <cstdlib>
#include <zlib.h>
#include <string.h>
#include <stdio.h>

#include "ZlibDecoder.h"

#include "types.h"
#include "Exception.h"

using namespace std;
using namespace doubrtom;

const unsigned int ZlibDecoder::CHUNK = 128000;

void ZlibDecoder::decode(U8 * src, U32 & srcLen, U8 *& dst, U32 & dstLen) {
	int ret;
	unsigned int have, avail;
	U8 * srcPointer = src;
	z_stream strm;
	
	buffer = new U8[CHUNK];
	bufferLen = CHUNK;
	dataLen = 0;
	
	/* allocate inflate state */
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.avail_in = 0;
	strm.next_in = Z_NULL;
	ret = inflateInit(&strm);
	checkError(ret, strm);
	
	do {
		if (srcLen == 0) break;
		
		strm.next_in = srcPointer;
		
		if (srcLen > CHUNK) {
			strm.avail_in = CHUNK;
			srcLen -= CHUNK;
			srcPointer += CHUNK;
		} else {
			strm.avail_in = srcLen;
			srcLen = 0;
			srcPointer = NULL;
		}
		
		do {
			avail = bufferLen - dataLen;
			strm.avail_out = avail;
			strm.next_out = buffer + dataLen;
			
			ret = inflate(&strm, Z_NO_FLUSH);
			checkError(ret, strm);
			
			have = avail - strm.avail_out;
			dataLen += have;
			avail = bufferLen - dataLen;
			
			if (avail == 0) increaseBufferSize();
			
		} while (strm.avail_out == 0);
		
	} while (ret != Z_STREAM_END);
	
	checkError(ret, strm);
	(void) inflateEnd(&strm);
	
	dst = buffer;
	dstLen = dataLen;
}

void ZlibDecoder::checkError(int ret, z_stream & strm) {
	switch (ret) {
		case Z_OK: /* fall throught */
		case Z_STREAM_END:
			printf("Success encoded.\n");
			break; /* not error => do nothing */
		case Z_STREAM_ERROR:
			error("stream error", strm);
			break;
		case Z_NEED_DICT:
			error("need dict", strm);
			break;
		case Z_DATA_ERROR:
			error("data error", strm);
			break;
		case Z_MEM_ERROR:
			error("memory error", strm);
			break;
		case Z_VERSION_ERROR:
			error("zlib version mismatch", strm);
			break;
		default:
			error("decode error", strm);
			break;
	}
}

void ZlibDecoder::error(const char * msg, z_stream & strm) {
	(void) inflateEnd(&strm);
	delete[] buffer;
	
	throw Exception(Exception::EX_ZLIB_DECODER, msg);
}

void ZlibDecoder::increaseBufferSize() {
	bufferLen *= 2;
	U8 * newBuffer = new U8[bufferLen];
	memcpy(newBuffer, buffer, dataLen);
	
	delete[] buffer;
	buffer = newBuffer;
}