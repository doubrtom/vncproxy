/* 
 * File:   ZlibDecoder.h
 * Author: tomas
 *
 * Created on 8. duben 2014, 22:00
 */

#ifndef ZLIBDECODER_H_876543456
#define	ZLIBDECODER_H_876543456

#include <zlib.h>
#include "types.h"

namespace doubrtom {

	/**
	 *	Class for decoding zlib compression.
	 */
	class ZlibDecoder {
	public:
		/**
		 * Decode src data into dst buffer.
		 * @param src	Data in zlib format.
		 * @param srcLen Length of zlib data in bytes.
		 * @param dst Decoded data.
		 * @param dstLen Length of decoded data.
		 */
		void decode(U8 * src, U32 & srcLen, U8 *& dst, U32 & dstLen);
		/**
		 * Increase size of buffer for decoding data.
		 */
		void increaseBufferSize();
		/**
		 * Check result of decoding.
		 * @param ret	Result value of decoding.
		 * @param strm Stream data.
		 */
		void checkError(int ret, z_stream & strm);
		/**
		 * Release memory and throw doubrtom::Exception.
     * @param msg		Error message
     */
		void error(const char * msg, z_stream & strm);
		
		
		static const unsigned int CHUNK;
	protected:
		U8 * buffer;
		U32 bufferLen;
		U32 dataLen;
		
	};
	
}

#endif	/* ZLIBDECODER_H_876543456 */

