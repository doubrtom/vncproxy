/* 
 * File:   types.h
 * Author: tomas
 *
 * Created on 18. únor 2014, 15:23
 */

#ifndef TYPES_H_8765456
#define	TYPES_H_8765456


namespace doubrtom {
	
	typedef unsigned char U8;
  typedef unsigned short U16;
  typedef unsigned int U32;
  typedef signed char S8;
  typedef signed short S16;
  typedef signed int S32;
	
	typedef U32 Pixel;
	
	typedef void (*fce_pointer)(void *);
	
}


#endif	/* TYPES_H_8765456 */

