/* 
 * File:   vncInfo.h
 * Author: tomas
 *
 * Created on 23. únor 2014, 0:12
 */

#ifndef VNCINFO_H_87654
#define	VNCINFO_H_87654

#include <cstdlib>

#include "types.h"

namespace doubrtom {
	
	/**
	 * Supported vnc security by vncproxy.
	 */
	struct VncSecurity {
		const static int NONE = 1;
		const static int VNC_AUTHENTICATION = 2;
	};
	
	/**
	 * Pixel format for transporting image data.
	 */
	struct PixelFormat {
		U8 bitsPerPixel;
		U8 depth;
		U8 bigEndianFlag;
		U8 trueColourFlag;
		U16 redMax;
		U16 greenMax;
		U16 blueMax;
		U8 redShift;
		U8 greenShift;
		U8 blueShift;
	};
	
	/**
	 * Data represent ...
	 */
	struct VncServerInfo {
		U16 framebufferWidth;
		U16 framebufferHeight;
		PixelFormat pixelFormat;
		U32 nameLength;
		U8 * name;
		
		VncServerInfo() {
			name = NULL;
		}
		~VncServerInfo() {
			if (name != NULL) delete[] name;
		}
	};
	
	/**
	 * Data represent Framebuffer update rectangle.
	 */
	struct Rectangle {
		U16 xPosition;
		U16 yPosition;
		U16 width;
		U16 height;
		S32 encodingType;
	};
	
}


#endif	/* VNCINFO_H */

