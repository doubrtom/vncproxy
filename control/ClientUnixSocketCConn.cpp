#include <stdlib.h>
#include <cstdio>
#include <cstring>
#include <errno.h>
#include <error.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>

#include "../common/Exception.h"
#include "ClientUnixSocketCConn.h"

using namespace std;
using namespace doubrtom;

ClientUnixSocketCConn::ClientUnixSocketCConn() {
	const char * home = getenv("HOME");
	if (home == NULL) {
		throw Exception("Missing environment variable HOME");
	}

	socketPath = new char[strlen(home) + 18];
	sprintf(socketPath, "%s/.vncproxy/socket", home);
	responseMsg = NULL;
}

ClientUnixSocketCConn::~ClientUnixSocketCConn() {
	delete[] socketPath;
	if (responseMsg != NULL) delete[] responseMsg;
	close(serverSocket);
}

void ClientUnixSocketCConn::connectToServer() {
	const char * errorMsg = "Unable to create control connection: %s";

	if ((serverSocket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		throw Exception(errorMsg, strerror(errno));
	}

	serverAddr.sun_family = AF_UNIX;
	strcpy(serverAddr.sun_path, socketPath);
	int len = strlen(serverAddr.sun_path) + sizeof (serverAddr.sun_family);
	if (connect(serverSocket, (sockaddr*) & serverAddr, len) == -1) {
		throw Exception(errorMsg, strerror(errno));
	}
}

void ClientUnixSocketCConn::processProgramArguments(int argc, char ** argv) {
	for (int i = 1; i < argc; i++) {
		if (argv[i][0] == '-' && i + 1 < argc && argv[i + 1][0] != '-') {
			processArgument(&(argv[i][1]), argv[i + 1]);
			i++;
		}
		if (argv[i][0] == '-') {
			processArgument(&(argv[i][1]), NULL);
		}
	}
}

void ClientUnixSocketCConn::processArgument(const char * key, const char * value) {
	if (!strcmp(key, "startrecord") || !strcmp(key, "sr")) {
		commandHeader[0] = START_RECORD;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "stoprecord") || !strcmp(key, "pr")) {
		commandHeader[0] = STOP_RECORD;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "startplayback") || !strcmp(key, "sp")) {
		commandHeader[0] = START_PLAYBACK;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "stopplayback") || !strcmp(key, "pp")) {
		commandHeader[0] = STOP_PLAYBACK;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "showstatus") || !strcmp(key, "s")) {
		commandHeader[0] = SHOW_STATUS;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "recordfile") || !strcmp(key, "rf")) {
		commandHeader[0] = SET_RECORD_FILE;
		setCommandArgument(key, value);
	} else if (!strcmp(key, "playbackspeed") || !strcmp(key, "pbs")) {
		commandHeader[0] = PLAYBACK_SPEED;
		setCommandArgument(key, value);
	} else if (!strcmp(key, "imagedir") || !strcmp(key, "id")) {
		commandHeader[0] = SET_IMAGE_DIR;
		setCommandArgument(key, value);
	} else if (!strcmp(key, "startscreencapture") || !strcmp(key, "ss")) {
		commandHeader[0] = START_SCREEN_CAPTURE;
		commandHeader[1] = 0;
	} else if (!strcmp(key, "stopscreencapture") || !strcmp(key, "ps")) {
		commandHeader[0] = STOP_SCREEN_CAPTURE;
		commandHeader[1] = 0;
	} else {
		throw Exception("Uknown parameter: %s", key);
	}

	sendCommand();
	printCommand(key);
	readResponse();
	printResponse();
}

void ClientUnixSocketCConn::sendCommand() {
	if (send(serverSocket, commandHeader, sizeof (commandHeader), 0) < 0) {
		throw Exception("Error sending command: %s", strerror(errno));
	}

	if (commandHeader[1] > 0) {
		if (send(serverSocket, arg, commandHeader[1], 0) < 0) {
			throw Exception("Error sending command: %s", strerror(errno));
		}
	}
}

void ClientUnixSocketCConn::readResponse() {
	if (recv(serverSocket, commandHeader, sizeof (commandHeader), 0) < 0) {
		throw Exception("Error reading response: %s", strerror(errno));
	}

	if (commandHeader[1] > 0) {
		responseMsg = new char[commandHeader[1] + 1];

		if (recv(serverSocket, responseMsg, commandHeader[1], 0) < 0) {
			throw Exception("Error sending command: %s", strerror(errno));
		}

		responseMsg[commandHeader[1]] = 0;
	}
}

void ClientUnixSocketCConn::printCommand(const char * cmd) {
	cout << "Processing " << cmd << ": ";
}

void ClientUnixSocketCConn::printResponse() {
	switch (commandHeader[0]) {
		case RESPONSE_OK:
			cout << "OK" << endl;
			break;
		case RESPONSE_FAIL:
			cout << "FAIL" << endl;
			break;
		case RESPONSE_ALREADY_SET:
			cout << "This value is already set." << endl;
			break;
		case RESPONSE_UNKNOWN_CMD:
			cout << "Unknown command." << endl;
			break;
		case RESPONSE_INVALID_ARGUMENT:
			cout << "Invalid argument." << endl;
			break;
		case RESPONSE_NOT_CONNECTED:
			cout << "Not connected to VNC." << endl;
			break;
		default:
			cout << "Unknown response command - " << commandHeader[0] << endl;
			break;
	}

	if (commandHeader[1] > 0) {
		cout << "\t" << responseMsg << endl;
		delete[] responseMsg;
		responseMsg = NULL;
	}
}

void ClientUnixSocketCConn::setCommandArgument(const char * cmd, const char * arg) {
	if (arg == NULL) {
		throw Exception("Missing argument for: %s.", cmd);
	}

	if (strlen(arg) > sizeof (this->arg)) {
		throw Exception("Command argument too long, max length is: %d", sizeof (this->arg));
	}

	commandHeader[1] = strlen(arg);
	strcpy(this->arg, arg);
}

const char * ClientUnixSocketCConn::getProgramName() {
	return this->programName;
}

void ClientUnixSocketCConn::setProgramName(const char* name) {
	this->programName = name;
}