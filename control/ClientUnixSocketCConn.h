/* 
 * File:   ClientUnixSocketCConn.h
 * Author: tomas
 *
 * Created on 18. únor 2014, 18:09
 */

#ifndef CLIENTUNIXSOCKETCCONN_H_5434567865456
#define	CLIENTUNIXSOCKETCCONN_H_5434567865456

#include "../common/UnixSocketCConn.h"

namespace doubrtom {
	
	/**
	 * Class representing client part of control communication.
   */
	class ClientUnixSocketCConn : public UnixSocketCConn {
	public:
		ClientUnixSocketCConn();
		~ClientUnixSocketCConn();
		
		void connectToServer();
		
		/**
		 * Parse CLI parameters.
     * @param argc	Program argument count
     * @param argv	Program arguments
     */
		void processProgramArguments(int argc, char ** argv);
		/**
		 * Call handler method for commands.
     * @param key		Name of command
     * @param value	Value of command
     */
		void processArgument(const char * key, const char * value);
		/**
		 * Send command to server.
     */
		void sendCommand();
		/**
		 * Read response from server.
     */
		void readResponse();
		/**
		 * Print processing command.
     * @param cmd		Name of command.
     */
		void printCommand(const char * cmd);
		/**
		 * Print response from server.
     */
		void printResponse();
		/**
		 * Check command argument from CLI and save it.
     * @param cmd	Name of command.
     * @param arg	Argument of command.
     */
		void setCommandArgument(const char * cmd, const char * arg);
		/**
		 * Set program name.
     * @param name	Program name.
     */
		void setProgramName(const char * name);
		/**
		 * Return program name.
     * @return Program name.
     */
		const char * getProgramName();
		
	protected:
		char * responseMsg;
		const char * programName;
		
	};
	
}


#endif	/* CLIENTUNIXSOCKETCCONN_H */

