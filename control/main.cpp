#include <cstdlib>
#include <iostream>

#include "../common/Exception.h"
#include "ClientUnixSocketCConn.h"

using namespace std;
using namespace doubrtom;

void printUsage(char ** argv) {
	cout << "usage: ";
	cout << argv[0] << " <OPTIONS>" << endl;
	cout << "example: " << argv[0] << " -startrecord -showstatus" << endl << endl;
	cout << endl;
	cout << "OPTIONS:" << endl;
	cout << "\t-startrecord, -sr" << endl;
	cout << "\t\tStart record VNC communication" << endl;
	cout << "\t-stoprecord, -pr" << endl;
	cout << "\t\tStop record VNC communication" << endl;
	cout << "\t-startplayback, -sp" << endl;
	cout << "\t\tStart playback VNC record." << endl;
	cout << "\t-stopplayback, -pp" << endl;
	cout << "\t\tStop playback VNC record" << endl;
	cout << "\t-showstatus, -s" << endl;
	cout << "\t\tShow status about VNC proxy setting." << endl;
	cout << "\t-recordfile, -rf" << endl;
	cout << "\t\tSet file for VNC recording" << endl;
	cout << "\t-playbackspeed, -ps" << endl;
	cout << "\t\tSet playback speed of VNC record" << endl;
	cout << "\t-imagedir, -id" << endl;
	cout << "\t\tSet image dir for screen captured images." << endl;
	cout << "\t-startscreencapture, -ss" << endl;
	cout << "\t\tStart screen capture of transporting VNC display." << endl;
	cout << "\t-stopscreencapture, -ps" << endl;
	cout << "\t\tStop screen capture of transporting VNC display." << endl;
}

/*
 * Main function of VNC proxy control
 */
int main(int argc, char** argv) {

	if (argc == 1) {
		printUsage(argv);
		return 0;
	}

	try {
		ClientUnixSocketCConn con;
		con.setProgramName(argv[0]);
		con.connectToServer();
		con.processProgramArguments(argc, argv);
	} catch (Exception & ex) {
		cerr << ex.getMessage() << endl;
		return 1;
	}

	return 0;
}
