#include <sstream>
#include <omp.h>

#include "CutTextRecord.h"
#include "../common/types.h"

using namespace std;
using namespace doubrtom;

CutTextRecord::CutTextRecord(U32 len, U8 * text) {
	this->text = new U8[len];
	this->length = len;
}

CutTextRecord::~CutTextRecord() {
	delete[] this->text;
}

string CutTextRecord::toString(double time) {
	stringstream ss;
	
	ss << "<" << TAGS_STRING[TAG_CUTTEXT] << " length=\"" << this->length
					<< "\" time=\"" << (omp_get_wtime() - time) << "\">" 
					<< this->text << "</" << TAGS_STRING[TAG_CUTTEXT] << ">";
	
	return ss.str();
}

U32 CutTextRecord::getLength() {
	return length;
}

U8 * CutTextRecord::getText() {
	return text;
}