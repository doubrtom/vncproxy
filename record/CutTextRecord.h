#ifndef CUTTEXTRECORD_H_8765434567
#define	CUTTEXTRECORD_H_8765434567

#include "../common/types.h"
#include "RecordItem.h"

namespace doubrtom {
	
	/**
	 * Class representing cutText tag.
   */
	class CutTextRecord : public RecordItem {
	public:
		/**
		 * Constructor
     * @param len		Length of text.
     * @param text	Cut text pointer.
     */
		CutTextRecord(U32 len, U8 * text);
		~CutTextRecord();
		
		std::string toString(double time);
		
		U32 getLength();
		U8 * getText();
		
	protected:
		U32 length;
		U8 * text;
	};
	
}

#endif	/* CUTTEXTRECORD_H_8765434567 */

