#include <sstream>
#include <omp.h>
#include <string.h>

#include "../common/KeyTranslator.h"
#include "KeyRecord.h"

using namespace std;
using namespace doubrtom;

KeyRecord::KeyRecord(U8 down, U32 key) {
	this->down = down;
	this->key = key;
}

KeyRecord::KeyRecord() {
	this->down = false;
	this->key = 0;
}

void KeyRecord::init(U8 down, U32 key) {
	this->down = down;
	this->key = key;
}

string KeyRecord::toString(double time) {
	stringstream ss;
	const char * keyName = KeyTranslator::translate(key);

	ss << "<" << TAGS_STRING[TAG_KEY];
	
	if (keyName[0] != 0) ss << " name=\"" << keyName << "\"";
					
	ss << " down=\"" 
					<< (int) down << "\" key=\"" << key << "\" time=\"" 
					<< (omp_get_wtime() - time) << "\" />";

	return ss.str();
}

U8 KeyRecord::getDown() {
	return down;
}

U32 KeyRecord::getKey() {
	return key;
}