#ifndef KEYRECORD_H_98765456
#define	KEYRECORD_H_98765456

#include "../common/types.h"
#include "RecordItem.h"

namespace doubrtom {
	
	/**
	 * Class representing key tag.
   * @param down
   * @param key
   */
	class KeyRecord : public RecordItem {
	public:
		/**
		 * Constructor.
     * @param down		Flag if key was pressed.
     * @param key			Code of key.
     */
		KeyRecord(U8 down, U32 key);
		KeyRecord();
		/**
		 * Init new state of class.
     * @param down		Flag if key was pressed.
     * @param key			Code of key.
     */
		void init(U8 down, U32 key);
		virtual std::string toString(double time);
		
		U8 getDown();
		U32 getKey();
		
	protected:
		U8 down;
		U32 key;
	};
	
}

#endif	/* KEYRECORD_H_98765456 */

