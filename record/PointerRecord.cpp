#include <sstream>
#include <omp.h>
#include <stdlib.h>
#include <cmath>

#include "PointerRecord.h"
#include "../common/types.h"

using namespace doubrtom;
using namespace std;

const int PointerRecord::PIXEL_PER_RECORD = 20;
const double PointerRecord::TIME_PER_RECORD = 0.1;

PointerRecord::PointerRecord(U8 buttonMask, U16 xPosition, U16 yPosition, double lastRecordTime) {
	this->buttonMask = buttonMask;
	this->xPosition = xPosition;
	this->yPosition = yPosition;
	
	pixel = 0;
	lastMoveTime = startTime = omp_get_wtime();
	this->lastRecordTime = lastRecordTime;
}
	
string PointerRecord::toString(double time) {
	stringstream ss;
	
	ss << "<" << TAGS_STRING[TAG_POINTER] 
					<< " btn=\"" << (int) buttonMask << "\" x=\"" 
					<< (int) xPosition << "\" y=\"" << yPosition << "\" time=\"" 
					<< (lastMoveTime - lastRecordTime) << "\" />";
	
	return ss.str();
}

/**
 * Add move of pointer to this record. If move distance is higher than RECORD_PER_PIXEL record is ready to save.
 * 
 * @param x		new x position of pointer
 * @param y		new y position of pointer
 * @return		true if record is ready to save or false
 */
void PointerRecord::addMove(U16 x, U16 y) {
	xPosition = x;
	yPosition = y;
	
	lastMoveTime = omp_get_wtime();
}

bool PointerRecord::isNewSample(U16 x, U16 y) {
	int width = x - xPosition;
	int height = y - yPosition;
	int dist = (int) sqrt((width*width) + (height*height));
	
	pixel += dist;
	
	double diff = omp_get_wtime() - startTime;
	
//	return (pixel > PointerRecord::RECORD_PER_PIXEL) ? true : false;
	return (diff > TIME_PER_RECORD || pixel > PIXEL_PER_RECORD) ? true : false;
}

U8 PointerRecord::getButtonMask() {
	return buttonMask;
}

U16 PointerRecord::getXPosition() {
	return xPosition;
}

U16 PointerRecord::getYPosition() {
	return yPosition;
}