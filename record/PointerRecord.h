#ifndef POINTERRECORD_H_87654345678765
#define	POINTERRECORD_H_87654345678765

#include "RecordItem.h"
#include "../common/types.h"


namespace doubrtom {
	
	/**
	 * Class representing pointer tag.
	 */
	class PointerRecord : public RecordItem {
	public:
		/**
		 * Constructor.
     * @param buttonMask		Mask of pressed buttons.
     * @param xPosition			Coordinate of absolute position of pointer.
     * @param yPosition			Coordinate of absolute position of pointer
     * @param lastRecordTime	Time of last created tag.
     */
		PointerRecord(U8 buttonMask, U16 xPosition, U16 yPosition, double lastRecordTime = 0);
		
		/**
		 * Check if new position is new record or only update.
     * @param x		Coordinate of new position.
     * @param y		Coordinate of new position.
     * @return		True if create new record, else false.
     */
		bool isNewSample(U16 x, U16 y);
		/**
		 * Update position of pointer.
     * @param x		Coordinate of new position.
     * @param y		Coordinate of new position.
     */
		void addMove(U16 x, U16 y);
		U8 getButtonMask();
		U16 getXPosition();
		U16 getYPosition();
		
		std::string toString(double time);
		
	protected:
		int pixel;
		double startTime;
		double lastMoveTime;
		double lastRecordTime;
		
		U8 buttonMask;
		U16 xPosition;
		U16 yPosition;
		
		static const int PIXEL_PER_RECORD;
		static const double TIME_PER_RECORD;
	};
	
}

#endif	/* POINTERRECORD_H_87654345678765 */

