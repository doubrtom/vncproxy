#include "RecordItem.h"

using namespace std;
using namespace doubrtom;

const char * RecordItem::TAGS_STRING[] = {
	"key", "setpixelformat", "setencoding", "encoding", "pointer",
	"cuttext", "update", "vncproxy", "text"
};
