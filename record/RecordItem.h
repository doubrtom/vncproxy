#ifndef RECORDITEM_H_23450987
#define	RECORDITEM_H_23450987

#include <string>

namespace doubrtom {
	
	/**
	 * Abstract parent for all tag classes.
	 */
	class RecordItem {
	public:
		
		enum Tags {
			TAG_KEY, TAG_SETPIXELFORMAT, TAG_SETENCODING, TAG_ENCODING, TAG_POINTER,
			TAG_CUTTEXT, TAG_UPDATE, TAG_VNCPROXY, TAG_TEXT
		};
		static const char * TAGS_STRING[];
		
		virtual ~RecordItem() {}
		virtual std::string toString(double time) = 0;
		
		
	};
	
}

#endif	/* RECORDITEM_H_23450987 */

