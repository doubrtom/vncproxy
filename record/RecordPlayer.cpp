#include <unistd.h>
#include <ctime>
#include <omp.h>
#include <string.h>
#include <string>

#include "RecordPlayer.h"
#include "../server/Server.h"
#include "KeyRecord.h"
#include "TextRecord.h"
#include "../server/VncClientToServer.h"
#include "../common/KeyTranslator.h"
#include "../common/Exception.h"
#include "../screen/Screen.h"
#include "../common/Cursor.h"

using namespace std;
using namespace doubrtom;

const double RecordPlayer::MAX_VARIANCE = 0.05;

RecordPlayer::RecordPlayer(Server & s) : server(s), c(s, "RecordPlayer") {
	running = false;
}

RecordPlayer::~RecordPlayer() {
	
}

void RecordPlayer::wait(double time) {
	if (time > variance) {
		if (variance > MAX_VARIANCE) {
			time -= variance;
			variance = 0;
		}
	} else if (time == variance) {
		variance = 0;
		return;
	} else {
		variance -= time;
		return;
	}
	
	timespec timeSleep, timeRemaining;
	double realWaitTime, beforeTime;
	
	timeSleep.tv_sec = (int) time;
	timeSleep.tv_nsec = (time - timeSleep.tv_sec) * 1000000000L;

	beforeTime = omp_get_wtime();

	nanosleep(&timeSleep, &timeRemaining);

	realWaitTime = omp_get_wtime() - beforeTime;
	variance += realWaitTime - time;
}

void RecordPlayer::logStatistics() {
  server.getLogger().log("Record time: %lf.", recordTime);
  server.getLogger().log("Playback time: %lf.", playbackTime);
}

void RecordPlayer::run() {
	recordReader.prepareReading();
	running = true;
	
	RecordItem * node;
	RecordItem::Tags tagType;
	double time;
	variance = 0.0;
	startTime = omp_get_wtime();
	recordTime = 0;

	while ((node = recordReader.getNextNode(tagType, time)) != NULL && server.getConfig().isReplaying() && server.isRunning() && running) {
		time *= server.getConfig().getPlaybackSpeed();
		recordTime += time;
		wait(time);
		processNode(node, tagType);
	}
	
	playbackTime = omp_get_wtime() - startTime;
	logStatistics();
}

void RecordPlayer::stop() {
	running = false;
}

Communicator & RecordPlayer::getCommunicator() {
	return c;
}

RecordReader & RecordPlayer::getRecordReader() {
	return recordReader;
}

void RecordPlayer::processNode(RecordItem * node, RecordItem::Tags tagType) {
	switch (tagType) {
		case RecordItem::TAG_KEY:
			processKeyRecord((KeyRecord *) node);
			break;
		case RecordItem::TAG_POINTER:
			processPointerRecord((PointerRecord *) node);
			break;
		case RecordItem::TAG_CUTTEXT:
			processCutTextRecord((CutTextRecord *) node);
			break;
		case RecordItem::TAG_TEXT:
			processTextRecord((TextRecord *) node);
			break;
		default:
			server.unlockToServerComm();
			throw Exception("Unknown tag.");
			break;
	}
	
	delete node;
}

void RecordPlayer::processKeyRecord(KeyRecord * record) {
	server.lockToServerComm();
	
	c.sendU8(static_cast<U8>(VncClientToServer::KEY_EVENT));
	c.sendU8(record->getDown());
	c.send(2); // padding
	c.sendU32(record->getKey());
	
	c.flushBuffer();
	
	server.unlockToServerComm();
}

void RecordPlayer::processTextRecord(TextRecord * record) {
	string textString = record->getTextString();
	const char * text = textString.c_str();
//	const char * text = record->getTextCString();
	KeyRecord keyRecord;
	U32 key;
	double waitTime;
	
	while (*text) {
		key = KeyTranslator::translate(*text);
		
		keyRecord.init(1, key);
		processKeyRecord(&keyRecord);
		
		keyRecord.init(0, key);
		processKeyRecord(&keyRecord);
		
		text++;
		if (*text) {
			waitTime = record->getTypeRating() * server.getConfig().getPlaybackSpeed();
			recordTime += waitTime;
			wait(waitTime);
		}
	}
}

void RecordPlayer::processPointerRecord(PointerRecord * record) {
	server.lockToServerComm();
	
	c.sendU8(static_cast<U8>(VncClientToServer::POINTER_EVENT));
	c.sendU8(record->getButtonMask());
	c.sendU16(record->getXPosition());
	c.sendU16(record->getYPosition());
	
	c.flushBuffer();
	
	server.unlockToServerComm();
	
	Screen & screen = server.getScreen();
	if (screen.isCursor()) screen.cursor->setPosition(record->getXPosition(), record->getYPosition());
}

void RecordPlayer::processCutTextRecord(CutTextRecord * record) {
	server.lockToServerComm();
	
	c.sendU8(static_cast<U8>(VncClientToServer::CLIENT_CUT_TEXT));
	c.send(3); // padding
	c.sendU32(record->getLength());
	c.send(record->getText(), record->getLength());
	
	c.flushBuffer();
	
	server.unlockToServerComm();
}