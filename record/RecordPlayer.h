#ifndef RECORDPLAYER_H_87654345678
#define	RECORDPLAYER_H_87654345678

#include "../common/Communicator.h"
#include "RecordReader.h"
#include "RecordItem.h"
#include "../common/types.h"

namespace doubrtom {
	
	class KeyRecord;
	class TextRecord;
	class CutTextRecord;
	class PointerRecord;
	
	/**
	 * Main class for record playback.
   */
	class RecordPlayer {
	public:
		RecordPlayer(Server & s);
		~RecordPlayer();
		
		/**
		 * Prepare class and start replaying. This method is better call from new thread for better timing in replaying.
     */
		void run();
		/**
		 * Stop replaying.
     */
		void stop();
		
		/**
		 * Method for waiting between two replaying tags. Contain logic for reduce latency.
     * @param time		Time from tag attribute.
     */
		void wait(double time);
		/**
		 * Log statistics from replaying into log file.
     */
		void logStatistics();
		
		Communicator & getCommunicator();
		RecordReader & getRecordReader();
		
		/**
		 * Process loaded tag and call appropriate record method.
     * @param node			Loaded tag.
     * @param tagType		Type of loaded tag.
     */
		void processNode(RecordItem * node, RecordItem::Tags tagType);
		/**
		 * Process key tag.
     * @param record		Loaded tag.
     */
		void processKeyRecord(KeyRecord * record);
		/**
		 * Process text record.
     * @param record		Loaded tag.
     */
		void processTextRecord(TextRecord * record);
		/**
		 * Process pointer tag.
     * @param record		Loaded tag.
     */
		void processPointerRecord(PointerRecord * record);
		/**
		 * Process cutText tag.
     * @param record		Loaded tag.
     */
		void processCutTextRecord(CutTextRecord * record);
		
		static const double MAX_VARIANCE;
	protected:
		Server & server;
		RecordReader recordReader;
		Communicator c;
		
		bool running;
		
		double startTime;
		double recordTime; // time of record -> sum of tag attribute time * timespeed
		double playbackTime; // time of playback -> real time of record playback
		double variance; // variance of real waiting from tag waiting in time attribute
	};
	
}

#endif	/* RECORDPLAYER_H_87654345678 */

