#include <fstream>
#include <string.h>
#include <string>
#include <cstdlib>
#include "../pugixml/pugixml.hpp"

#include "RecordReader.h"
#include "../common/Exception.h"
#include "KeyRecord.h"
#include "PointerRecord.h"
#include "CutTextRecord.h"
#include "TextRecord.h"

using namespace std;
using namespace doubrtom;

RecordReader::RecordReader() {
	ready = false;
}

RecordReader::RecordReader(const char * filePath) {
	setRecordFile(filePath);
}

void RecordReader::prepareReading() {
	iterator = doc.begin()->children().begin();
}

void RecordReader::setRecordFile(const char * filePath) {
	pugi::xml_parse_result result = doc.load_file(
			filePath, pugi::parse_default | pugi::parse_ws_pcdata_single
	);
	
	if (!result) {
		throw Exception("Cannot open record file: %s", result.description());
	}
	
	if (strcmp("vncproxy", doc.begin()->name())) {
		throw Exception("Main tag 'vncproxy' not found.");
	}
	
	ready = true;
}

bool RecordReader::isReady() {
	return ready;
}

void RecordReader::checkAttribute(const char * node, const char * attr) {
	if (attr[0] == 0) throw Exception(Exception::EX_BAD_XML_ATTRIBUTE, attr, node);
}

KeyRecord * RecordReader::createKeyRecord(pugi::xml_node & node) {
	const char * down, * key;
	
	key = node.attribute("key").value();
	down = node.attribute("down").value();
	checkAttribute(node.name(), key);
	checkAttribute(node.name(), down);
	
	return new KeyRecord(strToU8(down), strToU32(key));
}

PointerRecord * RecordReader::createPointerRecord(pugi::xml_node & node) {
	const char * button, * x, * y;
	
	button = node.attribute("btn").value();
	x = node.attribute("x").value();
	y = node.attribute("y").value();
	checkAttribute(node.name(), button);
	checkAttribute(node.name(), x);
	checkAttribute(node.name(), y);
	
	return new PointerRecord(strToU8(button), strToU16(x), strToU16(y));
}

CutTextRecord * RecordReader::createCutTextRecord(pugi::xml_node & node) {
	const char * lengthStr, * value;
	size_t length;
	
	lengthStr = node.attribute("length").value();
	value = node.text().get();
	checkAttribute(node.name(), lengthStr);
	length = strToU32(lengthStr);
	if (strlen(value) != length) {
		throw Exception("Incorrect length of cut text tag: length attribute is %s but content length is %d.",
						lengthStr, strlen(value)
		);
	}
	
	return new CutTextRecord(length, (U8 *) node.text().get());
}

TextRecord * RecordReader::createTextRecord(pugi::xml_node & node) {
	const char * typeRatingStr;
	
	typeRatingStr = node.attribute("typeRating").value();
	checkAttribute(node.name(), typeRatingStr);
	double typeRating = atof(typeRatingStr);
	if (typeRating == 0) throw Exception("Incorrect type rating in text node.");
	
	return new TextRecord(typeRating, node.text().get());
}

RecordItem * RecordReader::getNextNode(RecordItem::Tags & nodeType, double & time) {
	if (iterator == doc.begin()->children().end()) return NULL;
	
	pugi::xml_node node = *iterator;
	RecordItem * record = NULL;
	
	// get time
	const char * t = node.attribute("time").value();
	if (t[0] == 0) throw Exception("Missing mandatory tag parameter 'time' in node: %s.", node.name());
	time = atof(t);
	
	if (!strcmp(RecordItem::TAGS_STRING[RecordItem::TAG_KEY], node.name())) {
		nodeType = RecordItem::TAG_KEY;
		record = createKeyRecord(node);
	} else if (!strcmp(RecordItem::TAGS_STRING[RecordItem::TAG_POINTER], node.name())) {
		nodeType = RecordItem::TAG_POINTER;
		record = createPointerRecord(node);
	} else if (!strcmp(RecordItem::TAGS_STRING[RecordItem::TAG_CUTTEXT], node.name())) {
		nodeType = RecordItem::TAG_CUTTEXT;
		record = createCutTextRecord(node);
	} else if (!strcmp(RecordItem::TAGS_STRING[RecordItem::TAG_TEXT], node.name())) {
		nodeType = RecordItem::TAG_TEXT;
		record = createTextRecord(node);
	} else {
		throw Exception("Unsupported tag name: %s", node.name());
	}
	
	iterator++;
	return record;
}


/* ------------ convert methods ----------------------*/

U8 RecordReader::strToU8(const char * value) {
	return (U8) atoi(value);
}

U16 RecordReader::strToU16(const char * value) {
	return (U16) atoi(value);
}

U32 RecordReader::strToU32(const char * value) {
	return (U32) atoi(value);
}