#ifndef RECORDREADER_H_9876543
#define	RECORDREADER_H_9876543

#include <fstream>
#include "../pugixml/pugixml.hpp"

#include "RecordItem.h"
#include "PointerRecord.h"
#include "CutTextRecord.h"

namespace doubrtom {
	
	class KeyRecord;
	class PointerRecord;
	class CutTextRecord;
	class TextRecord;
	
	/**
	 * Class for reading XML record and parse it into tags.
	 */
	class RecordReader {
	public:
		RecordReader();
		/**
		 * Constructor.
     * @param filePath		File path to record file.
     */
		RecordReader(const char * filePath);
		
		/**
		 * Set record file.
     * @param filePath		File path to record file.
     */
		void setRecordFile(const char * filePath);
		/**
		 * Prepare for reading tags.
     */
		void prepareReading();
		/**
		 * Check if class is ready for reading, ie. has set record file.
     * @return 
     */
		bool isReady();
		/**
		 * Check mandatory attributes. If attr is empty string throw Exception.
     * @param node		Node for check.
     * @param attr		Attribute returned by PugiXML, contain attribute value or empty string.
     */
		void checkAttribute(const char * node, const char * attr);
		
		/**
		 * Read next tag from record file.
     * @param nodeType		Tag type.
     * @param time				Time for waiting.
     * @return						Next tag in record file.
     */
		RecordItem * getNextNode(RecordItem::Tags & nodeType, double & time);
		
		/**
		 * Create instance for key tag.
     * @param node		Loaded XML tag.
     * @return				New instance of tag class for loaded tag.
     */
		KeyRecord * createKeyRecord(pugi::xml_node & node);
		/**
		 * Create instance for pointer tag.
     * @param node		Loaded XML tag.
     * @return				New instance of tag class for loaded tag.
     */
		PointerRecord * createPointerRecord(pugi::xml_node & node);
		/**
		 * Create instance for cutText tag.
     * @param node		Loaded XML tag.
     * @return				New instance of tag class for loaded tag.
     */
		CutTextRecord * createCutTextRecord(pugi::xml_node & node);
		/**
		 * Create instance for record tag.
     * @param node		Loaded XML tag.
     * @return				New instance of tag class for loaded tag.
     */
		TextRecord * createTextRecord(pugi::xml_node & node);
		
		// convert methods
		U8 strToU8(const char * value);
		U16 strToU16(const char * value);
		U32 strToU32(const char * value);
		
		
	protected:
		pugi::xml_document doc;
		pugi::xml_node_iterator iterator;
		bool ready;
	};
	
}



#endif	/* RECORDREADER_H_9876543 */

