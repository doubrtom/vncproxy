#include <cstdlib>
#include <omp.h>
#include <fstream>

#include "RecordWriter.h"
#include "../common/Exception.h"
#include "../common/types.h"

#include "RecordItem.h"
#include "TextRecord.h"
#include "KeyRecord.h"
#include "PointerRecord.h"
#include "CutTextRecord.h"

using namespace std;
using namespace doubrtom;

RecordWriter::RecordWriter() {
	depth = 0;
	pointerRecord = NULL;
	textRecord = NULL;
}

RecordWriter::RecordWriter(const char * filePath) {
	setRecordFile(filePath);
	depth = 0;
	pointerRecord = NULL;
	textRecord = NULL;
}

RecordWriter::~RecordWriter() {
	stop();
}

void RecordWriter::write(RecordItem & item) {
	writeIndentation();
	file << item.toString(lastRecordTime) << endl;
}

void RecordWriter::start() {
	lastRecordTime = omp_get_wtime();
	writeHeader();
	depth++;
}

void RecordWriter::stop() {
	if (textRecord != NULL) writeText();
	if (pointerRecord != NULL) writePointer();
	
	writeFooter();
	
	depth = 0;
	file.close();
	file.clear();
}

void RecordWriter::writeHeader() {
	file << "<" << RecordItem::TAGS_STRING[RecordItem::TAG_VNCPROXY] 
					<< ">" << endl;
}

void RecordWriter::writeFooter() {
	file << "</" << RecordItem::TAGS_STRING[RecordItem::TAG_VNCPROXY] 
					<< ">" << endl;
}

void RecordWriter::writeIndentation() {
	for (int i=0; i<depth; i++) file << "\t";
}

void RecordWriter::setRecordFile(const char * filePath) {
	if (file.is_open()) {
		file.close();
		file.clear();
	}
	
	file.open(filePath, fstream::out);
	
	if (!file.is_open()) {
		throw Exception(Exception::EX_UNABLE_OPEN_FILE, filePath);
	}
}

bool RecordWriter::isReady() {
	return file.is_open();
}

/* ------ RECORDING METHOD -------------------- */

void RecordWriter::recordKey(U32 key, U8 down) {
	if (TextRecord::isTextChar(key)) {
		writeOut(TEXT);
		
		if (textRecord == NULL) textRecord = new TextRecord(2.0, lastRecordTime);
		
		if (textRecord->checkTypeRating()) {
			textRecord->log(key, down);
		} else {
			writeText();
			textRecord = new TextRecord(2.0, lastRecordTime);
			textRecord->log(key, down);
		}
	} else {
		writeOut(KEY);
		
		KeyRecord record(down, key);
		write(record);
	}
	
	lastRecordTime = omp_get_wtime();
}

void RecordWriter::recordPointer(U8 button, U16 x, U16 y) {
	writeOut(POINTER);
	
	if (pointerRecord == NULL) {
		pointerRecord = new PointerRecord(button, x, y, lastRecordTime);
//		writePointer(); // TODO del - test, immediate writeout
		lastRecordTime = omp_get_wtime();
		return;
	}
	
	if (pointerRecord->getButtonMask() == button) {
		if (pointerRecord->isNewSample(x, y)) {
			writePointer();
			pointerRecord = new PointerRecord(button, x, y, lastRecordTime);
		} else {
			pointerRecord->addMove(x, y);
		}
	} else {
		writePointer();
		pointerRecord = new PointerRecord(button, x, y, lastRecordTime);
	}
	
	lastRecordTime = omp_get_wtime();
}

void RecordWriter::recordCutText(U32 length, U8 * text) {
	writeOut(CUT_TEXT);
	
	CutTextRecord cutTextRecord(length, text);
	write(cutTextRecord);
	
	lastRecordTime = omp_get_wtime();
}

void RecordWriter::writeText() {
	write(*textRecord);
	delete textRecord;
	textRecord = NULL;
}

void RecordWriter::writePointer() {
	write(*pointerRecord);
	delete pointerRecord;
	pointerRecord = NULL;
}

void RecordWriter::writeOut(Record notWrite) {
	if (notWrite != TEXT && textRecord != NULL) writeText();
	if (notWrite != POINTER && pointerRecord != NULL) writePointer();
}