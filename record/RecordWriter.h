#ifndef RECORDWRITER_H_9876543456
#define	RECORDWRITER_H_9876543456

#include <fstream>

#include "../common/types.h"

namespace doubrtom {
	
	class TextRecord;
	class PointerRecord;
	class RecordItem;
	
	class RecordWriter {
	public:
		
		enum Record {
			KEY, POINTER, CUT_TEXT, TEXT
		};
		
		RecordWriter();
		~RecordWriter();
		/**
		 * Constructor.
     * @param filePath		File for writing new record.
     */
		RecordWriter(const char * filePath);
		/**
		 * Set record file.
     * @param filePath		File for writing new record.
     */
		void setRecordFile(const char * filePath);
		/**
		 * Check if class is ready for recording, ie. has set record file.
     * @return 
     */
		bool isReady();
		/**
		 * Write tag into file.
     * @param item		Class with next tag.
     */
		void write(RecordItem & item);
		/**
		 * Write XML header of record.
     */
		void writeHeader();
		/**
		 * Write XML footer of record.
     */
		void writeFooter();
		/**
		 * Prepare class for recording and start record timing.
     */
		void start();
		/**
		 * Stop recording.
     */
		void stop();
		
		/**
		 * Create class for key or text tag.
     * @param key		Key code
     * @param down	Flag if key was pressed or realesed.
     */
		void recordKey(U32 key, U8 down);
		/**
		 * Create or update class for pointer tag.
     * @param button		Mask of pressed buttons.
     * @param x					Coordinate of absolute position of pointer.
     * @param y					Coordinate of absolute position of pointer.
     */
		void recordPointer(U8 button, U16 x, U16 y);
		/**
		 * Create class for cutText tag.
     * @param length		Length of cut text.
     * @param text			Pointer to cut text.
     */
		void recordCutText(U32 length, U8 * text);
		
		/**
		 * Call for writing text tag into record file.
     */
		void writeText();
		/**
		 * Call for writing pointer tag into record file.
     */
		void writePointer();
		/**
		 * Call for writing tags in memory, like text and pointer.
     * @param notWrite
     */
		void writeOut(Record notWrite);
		
	protected:
		int depth;
		TextRecord * textRecord;
		PointerRecord * pointerRecord;
		std::fstream file;
		double lastRecordTime;
		
		void writeIndentation();
		
	};
	
}


#endif	/* RECORDWRITER_H_9876543456 */

