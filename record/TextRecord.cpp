#include <omp.h>

#define XK_LATIN1 true
#define XK_MISCELLANY true
#include <X11/keysymdef.h>

#include "../common/types.h"
#include "../common/KeyTranslator.h"
#include "TextRecord.h"

using namespace doubrtom;
using namespace std;


const int TextRecord::TYPERATING_TOLERANCE_KOEFICIENT = 3;

/* ----- STATIC METHOD ---------------- */

bool TextRecord::isTextChar(U32 button) {
	// latin characters
	if (button >= XK_space && button <= XK_asciitilde) return true;
	
	return false;
}

/* -------------- CONSTRUCTOR ------------ */

TextRecord::TextRecord(double typeRating, double lastRecordTime) : RecordItem() {
	this->typeRating = typeRating;
	startTime = lastKeyTime = omp_get_wtime();
	recordTime = startTime - lastRecordTime;
	counter = 0;
}

TextRecord::TextRecord(double typeRating, const char * str) {
	this->typeRating = typeRating;
	startTime = lastKeyTime = 0;
	recordTime = startTime = 0;
	counter = 0;
	
	ss << str;
}

/* ------------ METHOD ------------- */

void TextRecord::log(U32 button, U8 down) {
	if (down) {
		counter++; // next key pressed
	
		// time from last key
		double now = omp_get_wtime();
		lastKeyTime = now;

		if (counter > 10) typeRating = (now - startTime) / counter;
	}
	
	logKey(button, down);
}

void TextRecord::logKey(U32 button, U8 down) {
	// latin characters, log only pressed
	if (button >= XK_space && button <= XK_asciitilde && down) {
		ss << KeyTranslator::translate(button);
	}
}

string TextRecord::toString(double time) {
	typeRating = (lastKeyTime - startTime) / counter;
	
	stringstream ret;
	ret << "<" << TAGS_STRING[TAG_TEXT] << " typeRating=\"" << typeRating
					<< "\" time=\"" << recordTime << "\">";
	ret << ss.str();
	ret << "</" << TAGS_STRING[TAG_TEXT] << ">";
	
	return ret.str();
}

bool TextRecord::checkTypeRating() {
        double keyInterval = omp_get_wtime() - lastKeyTime;
	double tolerance = typeRating * TYPERATING_TOLERANCE_KOEFICIENT;
	return (keyInterval < tolerance);
}

double TextRecord::getTypeRating() {
	return typeRating;
}

std::string TextRecord::getTextString() {
	return ss.str();
}

const char * TextRecord::getTextCString() {
	return ss.str().c_str();
}