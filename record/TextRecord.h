#ifndef TEXTRECORD_H_987654567
#define	TEXTRECORD_H_87654567

#include <string>
#include <sstream>

#include "../common/types.h"
#include "RecordItem.h"

namespace doubrtom {
	
	/**
	 * Class representing text tag.
   */
	class TextRecord : public RecordItem {
	public:
		/**
		 * Static method for checking if pressed key is text character.
     * @param button	Button (Key) code
     * @return				True, if button is text key, or false.
     */
		static bool isTextChar(U32 button);

		/**
		 * Constructor.
     * @param typeRating				Default typerating.
     * @param lastRecordTime		Time of last create tag.
     */
		TextRecord(double typeRating, double lastRecordTime);
		/**
		 * Constructor.
     * @param typeRating				Default typerating.
     * @param str		Text in text tag.
     */
		TextRecord(double typeRating, const char * str);
		
		/**
		 * Method for log pressed/released key.
     * @param button		Button (Key) code.
     * @param down			Flag if pressed or released
     */
		void log(U32 button, U8 down);
		/**
		 * Check typerating.
     * @return	True if next key is in typerating, false if over.
     */
		bool checkTypeRating();
		
		virtual std::string toString(double time);
		
		double getTypeRating();
		std::string getTextString();
		const char * getTextCString();
		
	protected:
		double startTime;
		double lastKeyTime;
		double typeRating;
		double recordTime;
		unsigned int counter;
		std::stringstream ss;
		
		void logKey(U32 button, U8 down);
		
		static const int TYPERATING_TOLERANCE_KOEFICIENT;
	};
	
}

#endif	/* TEXTRECORD_H */

