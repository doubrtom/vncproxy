#include <cstdlib>
#include <stddef.h>
#include <string.h>
#include <cstdio>

#include "Screen.h"
#include "../common/Cursor.h"
#include "../common/Point.h"

using namespace doubrtom;
using namespace std;

Screen::Screen(int width, int height) {
	setScreen(width, height);
	cursor = NULL;
        index = 0;
}

Screen::Screen() {
	width = 0;
	height = 0;
	buffer = NULL;
	cursor = NULL;
        index = 0;
}

Screen::Screen(const Screen & s) {
	buffer = NULL;
	setScreen(s.width, s.height);
	memcpy(buffer, s.buffer, width * height * 3);
	
	if (s.cursor != NULL) cursor = new Cursor(*s.cursor);
	else cursor = NULL;
        
        index = 0;
}

Screen::~Screen() {
	if (buffer != NULL) delete[] buffer;
	if (cursor != NULL) delete cursor;
}

Screen & Screen::operator=(const Screen & s) {
	if (this == &s) return *this;
	
	setScreen(s.width, s.height);
	memcpy(buffer, s.buffer, width * height * 3);
	
	if (s.cursor != NULL) {
		if (cursor == NULL) cursor = new Cursor(*s.cursor);
		else *cursor = *s.cursor;
	} else {
		if (cursor != NULL) deleteCursor();
		else cursor = NULL;
	}
	
	return *this;
}

void Screen::setScreen(int width, int height) {
	this->width = width;
	this->height = height;

	if (buffer != NULL) delete[] buffer;
	buffer = new U8[width * height * 3];
}

void Screen::setCursor(Cursor * cursor) {
	if (this->cursor != NULL) delete this->cursor;
	this->cursor = cursor;
}

void Screen::setIndex(unsigned long int index) {
    this->index = index;
}

unsigned long int Screen::getIndex() {
    return index;
}

bool Screen::isCursor() {
	return (cursor != NULL);
}

void Screen::deleteCursor() {
	if (cursor != NULL) {
		delete cursor;
		cursor = NULL;
	}
}

void Screen::renderCursor() {
	if (cursor == NULL || buffer == NULL) return;

	Point renderPixel(cursor->position.x - cursor->dimensions.x, cursor->position.y - cursor->dimensions.y);

	U8 * dst = buffer + (renderPixel.y * width * 3) + (renderPixel.x * 3);
  U8 * maxDst = buffer + (width * height * 3);
	U8 * src = cursor->buffer;
	U8 * bitmask = cursor->bitmask;
	U8 mask = 128;
	bool inBuffer;

	for (int i=0; i < cursor->dimensions.height; i++) {
		renderPixel.x = cursor->position.x - cursor->dimensions.x;

		for (int j=0; j < cursor->dimensions.width; j++) {
			inBuffer = (renderPixel.x >= 0 && renderPixel.x <= width) && (renderPixel.y >= 0 && renderPixel.y <= height);
      
			if (((*bitmask) & mask) && inBuffer && (dst < maxDst) && (dst >= buffer)) {
				memcpy(dst, src, 3);
			}
			
			mask /= 2;
			dst += 3;
			src += 3;
			renderPixel.x++;
			
			if (mask == 1) {
				bitmask++;
				mask = 128;
			}
		}
		
		if (mask != 128) {
			bitmask++;
			mask = 128;
		}
		
		dst += (width - cursor->dimensions.width) * 3;
		renderPixel.y++;
	}
}