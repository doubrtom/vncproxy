#ifndef SCREEN_H_87654567
#define	SCREEN_H_87654567

#include "../common/types.h"

namespace doubrtom {

	class Cursor;

	/**
	 * Class representing framebuffer for transmitted image data.
   * @param width
   * @param height
   */
	class Screen {
	public:
		/**
		 * Constructor.
     * @param width		Width of framebuffer.
     * @param height	Height of framebuffer.
     */
		Screen(int width, int height);
		Screen();
		Screen(const Screen & s);
		~Screen();

		Screen & operator=(const Screen & s);

		/**
		 * Set new width and height of screen.
     * @param width		New width.
     * @param height	New height.
     */
		void setScreen(int width, int height);
		void setCursor(Cursor * cursor);
		void setIndex(unsigned long int index);
		unsigned long int getIndex();

		/**
		 * Check if screen has set cursor.
     * @return	True, if cursor is set, else false.
     */
		bool isCursor();
		/**
		 * Delte cursor data.
     */
		void deleteCursor();
		/**
		 * Render cursor into framebuffer.
     */
		void renderCursor();


		int width;
		int height;
		U8 * buffer;
		Cursor * cursor;

		unsigned long int index;
	};

}

#endif	/* SCREEN_H_87654567 */

