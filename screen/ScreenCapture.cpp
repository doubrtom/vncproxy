#include <queue>
#include <pthread.h>
#include <iostream>

#include "ScreenCapture.h"
#include "Screen.h"
#include "../common/Exception.h"
#include "ScreenSaver.h"
#include "../server/Server.h"

using namespace std;
using namespace doubrtom;

ScreenCapture::ScreenCapture(Server & s) : server(s) {
    running = false;
    cleanState = true;

    pthread_mutex_init(&queueMutex, NULL);
    pthread_mutex_init(&runningMutex, NULL);
    sem_init(&taskSem, 0, 0);

    imageCounter = 0;
}

ScreenCapture::~ScreenCapture() {
    if (!cleanState) stop();

    pthread_mutex_destroy(&queueMutex);
    pthread_mutex_destroy(&runningMutex);
    sem_destroy(&taskSem);
}

void ScreenCapture::init() {
    logger.init(server.getConfig().getImageDir());

    poolSize = server.getThreadPoolSize();
    running = true;
    cleanState = false;

    threads = new pthread_t[poolSize];
    threadsId = new int[poolSize];

    clearTasks();
    sem_destroy(&taskSem);
    sem_init(&taskSem, 0, 0);
    
    imageCounter = 0;
}

void ScreenCapture::cleanUp() {
    delete[] threads;
    delete[] threadsId;
    threads = NULL;
    threadsId = NULL;
    cleanState = true;
}

/* ------- public methods ----------- */

void ScreenCapture::start() {
    init();
    logger.logStart();

    try {
        createThreads();
    } catch (Exception & ex) {
        cleanUp();
        throw ex;
    }
}

void ScreenCapture::stop() {
    logger.logEnd();
    logger.stop();
    
    setRunning(false);

    stopThreads();
		
		server.getLogger().log("Screen capture: Total number of threads - %d.", poolSize);
		server.getLogger().log("Screen capture: Total number of images - %d.", imageCounter-1);

    cleanUp();
}

void ScreenCapture::logNextImage(unsigned long int index) {
    logger.logImage(index);
}

void ScreenCapture::clearTasks() {
    pthread_mutex_lock(&queueMutex);

    std::queue<Screen *> empty;
    std::swap(queue, empty);

    pthread_mutex_unlock(&queueMutex);
}

void ScreenCapture::addTask(Screen * screen) {
    pthread_mutex_lock(&queueMutex);

    screen->setIndex(imageCounter);
    logNextImage(imageCounter);
    imageCounter++;
    
    queue.push(screen);
    sem_post(&taskSem);
//    printf("Task pridan\n");

    pthread_mutex_unlock(&queueMutex);
}

Screen * ScreenCapture::getTask() {
    pthread_mutex_lock(&queueMutex);

    Screen * screen = queue.front();
    queue.pop();
//    printf("task vyzvednut\n");

    pthread_mutex_unlock(&queueMutex);

    return screen;
}

bool ScreenCapture::hasTask() {
    pthread_mutex_lock(&queueMutex);

    long unsigned int i = queue.size();

    pthread_mutex_unlock(&queueMutex);

    return (i != 0);
}

bool ScreenCapture::isRunning() {
    pthread_mutex_lock(&runningMutex);

    bool tmp = running;

    pthread_mutex_unlock(&runningMutex);

    return tmp;
}

void ScreenCapture::setRunning(bool val) {
    pthread_mutex_lock(&runningMutex);

    running = val;

    pthread_mutex_unlock(&runningMutex);
}

/* ----- help functions ------ */

namespace doubrtom {

    void startThread(ScreenCapture * screenCapture) {
				screenCapture->server.getLogger().log("Screen capture: Work thread created.");

        while (screenCapture->isRunning() || screenCapture->hasTask()) {
            sem_wait(&screenCapture->taskSem);

            if (!screenCapture->isRunning() && !screenCapture->hasTask()) {
                sem_post(&screenCapture->taskSem);
                break;
            }

            Screen * screen = screenCapture->getTask();
            screen->renderCursor();
            ScreenSaver saver(screenCapture->server);
            saver.saveScreen(*screen);

            delete screen;
        }
				
        screenCapture->server.getLogger().log("Screen capture: Work thread closed.");
    }

}

/* ------------ protected methods ------------- */

void ScreenCapture::createThreads() {
    for (int i = 0; i < poolSize; i++) {
        threadsId[i] = pthread_create(&threads[i], NULL, (void * (*)(void *)) startThread, (void *) this);
        checkThread(i);
    }
}

void ScreenCapture::checkThread(int index) {
    if (threadsId[index] == 0) return;

    stopThreads(index);

    throw Exception(Exception::EX_CREATE_THREAD, "Screen capture");
}

void ScreenCapture::stopThreads(int count) {
    if (count < 1) return;

    setRunning(false);

    sem_post(&taskSem);

    for (int i = 0; i < count; i++) {
        pthread_join(threads[i], NULL);
    }
}

void ScreenCapture::stopThreads() {
    stopThreads(poolSize);
}

