#ifndef SCREENCAPTURE_H_876545678
#define	SCREENCAPTURE_H_876545678

#include <queue>
#include <pthread.h>
#include <semaphore.h>
#include "ScreenLogger.h"

namespace doubrtom {

	class Screen;
	class Server;

	/**
	 * Main class of screen capture mode. 
	 * Contain logic for multi-thread saving of PNG images, princip producer-consumer.
   */
	class ScreenCapture {
	public:
		ScreenCapture(Server & s);
		~ScreenCapture();

		/**
		 * Start capturing of screen.
     */
		void start();
		/**
		 * Stop capturing of screen.
     */
		void stop();

		/**
		 * Add task into queue for processing.
     * @param screen		Screen with framebuffer for saving.
     */
		void addTask(Screen * screen);
		/**
		 * Return task from queue.
     * @return 
     */
		Screen * getTask();
		/**
		 * Check if queue contains any task.
     * @return		If queue is not empty return true, else false
     */
		bool hasTask();
		/**
		 * Remove all tasks from queue.
     */
		void clearTasks();
		/**
		 * Log next image into screen capture log.
     * @param index			Index of saving/loging image.
     */
		void logNextImage(unsigned long int index);

		/**
		 * Check if screen capture is running.
     * @return		True, if capturing, else false.
     */
		bool isRunning();
		void setRunning(bool val);

		/**
		 * Friend method for start new (work-consumer) thread.
     */
		friend void startThread(ScreenCapture * screenCapture);
	protected:
		/**
		 * Init class for screen capturing.
     */
		void init();
		/**
		 * Clear class state and data.
     */
		void cleanUp();
		/**
		 * Create work-consumer threads.
     */
		void createThreads();
		/**
		 * Check if thread was created successfully.
     * @param index
     */
		void checkThread(int index);
		/**
		 * Stop all work-consumer threads.
     */
		void stopThreads();
		/**
		 * Stop count wok-consumer threads.
     * @param count
     */
		void stopThreads(int count);

		sem_t taskSem;
		pthread_mutex_t queueMutex;
		pthread_mutex_t runningMutex;
		pthread_t * threads;
		int * threadsId;

		int poolSize;
		bool running;
		bool cleanState;
		std::queue<Screen *> queue;

		unsigned long int imageCounter;

		Server & server;
		ScreenLogger logger;
	};

}

#endif	/* SCREENCAPTURE_H_876545678 */

