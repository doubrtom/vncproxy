#include <iostream>
#include <fstream>
#include <omp.h>

#include "ScreenLogger.h"
#include "../common/Exception.h"

using namespace doubrtom;
using namespace std;

ScreenLogger::ScreenLogger() {
    lastLogTime = 0;
}

ScreenLogger::~ScreenLogger() {
    
}

void ScreenLogger::init(const char * imageDir) {
    char filePath[512];

    sprintf(filePath, "%s/log.txt", imageDir);
    file.open(filePath, fstream::out);
    
    if (!file.is_open()) {
        throw Exception("Unable to create log file for screen capturing.");
    }
    
    lastLogTime = omp_get_wtime();
}

void ScreenLogger::stop() {
    if (file.is_open()) file.close();
}

void ScreenLogger::logImage(long int index) {
    double now = omp_get_wtime();
    
    file << (now - lastLogTime) << ": " <<  index << ".png" << endl;
    
    lastLogTime = now;
}

void ScreenLogger::logStart() {
    
}

void ScreenLogger::logEnd() {

}

