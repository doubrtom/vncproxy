#ifndef SCREEN_LOGGER_H_8652829344
#define	SCREEN_LOGGER_H_8652829344

#include <fstream>

namespace doubrtom {

		/**
		 * Class for generating log from screen capturing.
		 */
    class ScreenLogger {
    public:
        ScreenLogger();
        ~ScreenLogger();
        
				/**
				 * Init log.
         * @param dir		Dir for log file, ie. 'imagedir'.
         */
        void init(const char * dir);
				/**
				 * Stop logging.
         */
        void stop();

				/**
				 * Log next image.
         * @param index		Index of saved image.
         */
        void logImage(long int index);
				/**
				 * Log header of file.
         */
        void logStart();
				/**
				 * Log footer of file.
         */
        void logEnd();

    protected:
        std::fstream file;
        double lastLogTime;

    };

}


#endif	/* SCREEN_H_8652829344 */

