#include <stdio.h>
#define PNG_DEBUG 3
#include <png.h>
#include <libpng12/png.h>

#include "ScreenSaver.h"
#include "Screen.h"
#include "../server/Server.h"
#include "../common/Exception.h"

using namespace std;
using namespace doubrtom;

ScreenSaver::ScreenSaver(Server & s) : server(s) {
	rowPointers = NULL;
}

ScreenSaver::~ScreenSaver() {
}

void ScreenSaver::saveScreen(Screen & screen) {
	// create new file
	openFile(screen);
	
	// init
	png_structp pngPointer = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pngPointer) {
		png_destroy_write_struct(&pngPointer, NULL);
		throw Exception("Unable create png write structure.");
	}
	
	png_infop infoPointer = png_create_info_struct(pngPointer);
	if (!infoPointer) {
		png_destroy_write_struct(&pngPointer, &infoPointer);
		throw Exception("Unable create png info structure.");
	}
	
	if (setjmp(png_jmpbuf(pngPointer))) {
		png_destroy_write_struct(&pngPointer, &infoPointer);
		throw Exception("Error during init png io.");
	}
	png_init_io(pngPointer, file);
	
	// write header
	if (setjmp(png_jmpbuf(pngPointer))) {
		png_destroy_write_struct(&pngPointer, &infoPointer);
		throw Exception("Error during writing png header.");
	}
	png_set_IHDR(
			pngPointer, infoPointer, screen.width, screen.height, 8,
			PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, 
			PNG_FILTER_TYPE_BASE
	);
	png_write_info(pngPointer, infoPointer);
	
	// write data
	if (setjmp(png_jmpbuf(pngPointer))) {
		png_destroy_write_struct(&pngPointer, &infoPointer);
		throw Exception("Error during writing png file.");
	}
	prepareData(screen);
	png_write_image(pngPointer, rowPointers);
	
	// end write
	if (setjmp(png_jmpbuf(pngPointer))) {
		png_destroy_write_struct(&pngPointer, &infoPointer);
		throw Exception("Error during end of writing.");
	}
	png_write_end(pngPointer, NULL);
	
	// clean up memory
	png_destroy_write_struct(&pngPointer, &infoPointer);
	
	cleaneData();
	fclose(file);
}

void ScreenSaver::prepareData(Screen & screen) {
	U8 * screenPointer = (U8 *) screen.buffer;
	rowPointers = new png_byte*[screen.height];
	
	for (int i=0; i<screen.height; i++) {
		rowPointers[i] = (png_byte *) screenPointer;
		screenPointer += screen.width * 3;
	}
}

void ScreenSaver::cleaneData() {
	if (rowPointers != NULL) delete[] rowPointers;
}

void ScreenSaver::openFile(Screen & screen) {
	char fileName[300];
	sprintf(fileName, "%s/%06ld.png", server.getConfig().getImageDir(), screen.getIndex());
	file = fopen(fileName, "wb");
	if (!file) throw Exception(Exception::EX_SCREEN_SAVER_UNABLE_CREATE_FILE);
}
