#ifndef SCREENSAVER_H_87654345678
#define	SCREENSAVER_H_87654345678

#include <stdio.h>
#include <png.h>

#include "../common/types.h"

namespace doubrtom {
	
	class Server;
	class Screen;
	
	/**
	 * Class for saving framebuffer data into PNG file.
   * @param s
   */
	class ScreenSaver {
	public:
		ScreenSaver(Server & s);
		~ScreenSaver();
		
		/**
		 * Save screen into PNG file.
     * @param screen		Screen with framebuffer data.
     */
		void saveScreen(Screen & screen);
		
	protected:
		::FILE * file;
		::png_byte ** rowPointers;
		
		Server & server;
		
		/**
		 * Prepare data into PNG format.
     * @param screen
     */
		void prepareData(Screen & screen);
		/**
		 * Clean all data.
     */
		void cleaneData();
		/**
		 * Open file for writing PNG file.
     * @param screen
     */
		void openFile(Screen & screen);
	};
	
}

#endif	/* SCREENSAVER_H_87654345678 */

