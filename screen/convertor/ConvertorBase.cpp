#include <netinet/in.h>
#include <cstring>
#include <cstdlib>

#include "ConvertorBase.h"

using namespace doubrtom;
using namespace std;

ConvertorBase::ConvertorBase(void * buffer, U16 width, U16 height, PixelFormat & format) {
	this->screenBuffer = (U8 *) buffer;
	this->screenWidth = width;
	this->screenHeight = height;
	this->pixelFormat = format;
	
	isBigEndian = (htonl(47) == 47);
}

void ConvertorBase::setRect(int x, int y, int width, int height) {
	r.set(x, y, width, height);
}

void ConvertorBase::setRect(Rect & r) {
	this->r = r;
}

void ConvertorBase::setScreenBuffer(U8 * buffer) {
	this->screenBuffer = buffer;
}

void ConvertorBase::setPixelFormat(PixelFormat & format) {
	this->pixelFormat = format;
}

void ConvertorBase::setScreenWidth(U16 width){
	this->screenWidth = width;
}

void ConvertorBase::setScreenHeight(U16 height) {
	this->screenHeight = height;
}

void ConvertorBase::setScreenDimension(U16 width, U16 height) {
	this->screenWidth = width;
	this->screenHeight = height;
}