#ifndef CONVERTORBASE_H_87656776545
#define	CONVERTORBASE_H_87656776545

#include "../../common/types.h"
#include "../../common/Rect.h"
#include "../../common/vncInfo.h"

namespace doubrtom {
	
	/**
	 * Base class for colour convertors.
   */
	class ConvertorBase {
	public:
		/**
		 * Constructor.
     * @param buffer	Buffer with screen data.
     * @param width		Screen width.
     * @param height	Screen height.
     * @param format	Pixel format selected by VNC client.
     */
		ConvertorBase(void * buffer, U16 width, U16 height, PixelFormat & format);
		virtual ~ConvertorBase() {}
		
		/**
		 * Set rectangle for next image data.
     * @param x			Coordinate of rectangle.
     * @param y			Coordinate of rectangle.
     * @param width	Width of rectangle.
     * @param height	Height or rectangle.
     */
		void setRect(int x, int y, int width, int height);
		/**
		 * Set rectangle for next image data.
     * @param rect		Rectangle with new data.
     */
		void setRect(Rect & r);
		void setScreenBuffer(U8 * buffer);
		void setPixelFormat(PixelFormat & format);
		void setScreenWidth(U16 width);
		void setScreenHeight(U16 height);
		void setScreenDimension(U16 width, U16 height);
		
		/**
		 * Copy image data from buffer into rectangle specified by setRect.
     * @param buffer		Image data.
     */
		virtual void copyRect(void * buffer) = 0;
		/**
		 * Copy image data specified by setRect from screen buffer into different position in screen buffer. Encoding used for scrolling or moving with window.
     * @param srcX	Source coordinate of copyiing rectangle.
     * @param srcY	Source coordinate of copyiing rectangle.
     */
		virtual void copyRect(U16 srcX, U16 srcY) = 0;
		/**
		 * Copy image data from buffer into rectangle specified by r parameter.
     * @param r					Subrectangle data.
     * @param buffer		Image data.
     */
		virtual void copyRect(Rect & r, void * buffer) = 0;
		/**
		 * Set pixels in rectangle specified by r parameter to px value.
     * @param r			Subrectangle data.
     * @param px		Pixel with new color.
     */
		virtual void setPixels(Rect & r, Pixel px) = 0;
		/**
		 * Set pixels of whole updating rectangle for px value.
     * @param px
     */
		virtual void setPixels(Pixel px) = 0;
		
	protected:
		U8 * screenBuffer;
		U16 screenWidth;
		U16 screenHeight;
		Rect r;
		PixelFormat pixelFormat;
		bool isBigEndian;
		
		
	};
	
}

#endif	/* CONVERTORBASE_H_87656776545 */

