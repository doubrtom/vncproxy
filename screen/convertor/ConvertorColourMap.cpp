#include <cstring>

#include "ConvertorColourMap.h"

#include "../../common/Exception.h"

using namespace doubrtom;


ConvertorColourMap::ConvertorColourMap(void * buffer, U16 width, U16 height, PixelFormat & format)
: ConvertorBase(buffer, width, height, format) {
	colourMap = NULL;
}

ConvertorColourMap::~ConvertorColourMap() {
	if (colourMap != NULL) delete[] colourMap;
}


void ConvertorColourMap::createColourMap(U16 size){
	if (colourMap != NULL) delete[] colourMap;
	
	colourMap = new Pixel[size];
	colourMapSize = size;
}

void ConvertorColourMap::clearColourMap() {
	if (colourMap != NULL) delete[] colourMap;
}

void ConvertorColourMap::insertColour(U16 index, Pixel colour) {
	if (colourMap == NULL) throw Exception("Colour map not init.");
	
	colourMap[index] = colour;
}

void ConvertorColourMap::insertColour(U16 index, U16 red, U16 green, U16 blue) {
	if (colourMap == NULL) throw Exception("Colour map not init.");
	
	U8 r = (U8) red;
	U8 g = (U8) green;
	U8 b = (U8) blue;
	
	colourMap[index] = (r << 16) & (g << 8) & (b);
}

Pixel ConvertorColourMap::getColour(U16 index) {
	if (colourMap == NULL) throw Exception("Colour map is empty.");
	
	if (index >= colourMapSize) throw Exception("Colour map: Colour index out of range.");
	return colourMap[index];
}

Pixel ConvertorColourMap::getColour(void * pointer) {
	Pixel index;
	
	switch (pixelFormat.bitsPerPixel) {
		case 8:
			index = (Pixel) *((U8 *) pointer);
			break;
		case 16:
			if (isBigEndian != pixelFormat.bigEndianFlag) index = (Pixel) __bswap_16(*((U16 *) pointer));
			else index = (Pixel) *((U16 *) pointer);
			break;
		case 32:
			index = (Pixel) *((U32 *) pointer);
			if (isBigEndian != pixelFormat.bigEndianFlag) index = (Pixel) __bswap_32(index);
			break;
		default:
			throw Exception("Unsupported bits per pixel value: %d.", pixelFormat.bitsPerPixel);
	}
	
	if (index >= colourMapSize) throw Exception("Colour map: Colour index out of range.");
	return colourMap[index];
}
		
void ConvertorColourMap::setColour(U8 * buffer, Pixel colour) {
	*(buffer++) = (colour >> 16) & 255;
	*(buffer++) = (colour >> 8) & 255;
	*(buffer++) = (colour) & 255;
}



// parent's methods

void ConvertorColourMap::copyRect(void * buffer) {
	copyRect(this->r, buffer);
}

void ConvertorColourMap::copyRect(U16 srcX, U16 srcY) {
	U8 * copyBuffer = new U8[r.width * r.height * 3];
	U8 * dstPointer = copyBuffer;
	U8 * srcPointer = this->screenBuffer + (screenWidth*srcY*3) + (srcX*3);
	
	for (int i=0; i<r.height; i++) {
		memcpy(dstPointer, srcPointer, r.width * 3);
		dstPointer += r.width * 3;
		srcPointer += screenWidth * 3;
	}
	
	dstPointer = this->screenBuffer + (screenWidth*r.y*3) + (r.x*3);
	srcPointer = copyBuffer;
	
	for (int i=0; i<r.height; i++) {
		memcpy(dstPointer, srcPointer, r.width * 3);
		dstPointer += screenWidth * 3;
		srcPointer += r.width * 3;
	}

	delete[] copyBuffer;
}

void ConvertorColourMap::copyRect(Rect & r, void * buffer) {
	U8 * rowSrc = (U8 *) buffer;
	U8 * rowDst = this->screenBuffer + (this->screenWidth*r.y*3) + (r.x*3);
	
	for (int i=0; i < r.height; i++) {
		for (int j=0; j < r.width; j++) {
			Pixel px = getColour((void *) rowSrc);
			setColour(rowDst, px);
			
			rowSrc += pixelFormat.bitsPerPixel/8;
		}
		
		rowDst += (this->screenWidth - r.width) * 3;
	}
}

void ConvertorColourMap::setPixels(Rect & r, Pixel px) {
	U8 * row = this->screenBuffer + (this->screenWidth*r.y*3) + (r.x*3);
	Pixel colour;

	for (int i=0; i<r.height; i++) {
		for (int j=0; j<r.width; j++) {
			colour = getColour((U16) px);
			setColour(row, colour);
		}
		
		row += (this->screenWidth - r.width) * 3;
	}
}

void ConvertorColourMap::setPixels(Pixel px) {
	setPixels(this->r, px);
}
