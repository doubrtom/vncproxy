#ifndef CONVERTORCOLOURMAP_H
#define	CONVERTORCOLOURMAP_H

#include "ConvertorBase.h"
#include "../../common/types.h"

namespace doubrtom {
	
	/**
	 * Class for converting colour map into RGB888 format.
   */
	class ConvertorColourMap : public ConvertorBase {
	public:
		/**
		 * Constructor
     * @see ConvertorBase::ConvertorBase(void * buffer, U16 width, U16 height, PixelFormat & format)
     */
		ConvertorColourMap(void * buffer, U16 width, U16 height, PixelFormat & format);
		~ConvertorColourMap();
		
		/**
		 * Create colour map of specified size.
     * @param size	Size of new colur map.
     */
		void createColourMap(U16 size);
		/**
		 * Clear data of colour map.
     */
		void clearColourMap();
		/**
		 * Insert colour into colour map.
     * @param index		Index of colour.
     * @param colour	Coulour data.
     */
		void insertColour(U16 index, Pixel colour);
		/**
		 * Insert colour into colour map.
     * @param index		Index of colour.
     * @param red			Red part of colour.
     * @param green		Green part of colour.
     * @param blue		Blue part of colour.
     */
		void insertColour(U16 index, U16 red, U16 green, U16 blue);
		
		/**
		 * Return colour from colour map.
     * @param index		Index of colour.
     * @return				Colour data.
     */
		Pixel getColour(U16 index);
		/**
		 * Get colour data from pointer destination.
     * @param pointer		Pointer to colour data.
     * @return					Colour data.
     */
		Pixel getColour(void * pointer);
		
		/**
		 * Set colour in buffer to colour data.
     * @param buffer		Buffer for setting colour.
     * @param colour		Colour.
     */
		void setColour(U8 * buffer, Pixel colour);
		
		
		// parent methods
		virtual void copyRect(void * buffer);
		virtual void copyRect(U16 srcX, U16 srcY);
		virtual void copyRect(Rect & r, void * buffer);
		virtual void setPixels(Rect & r, Pixel px);
		virtual void setPixels(Pixel px);
		
	protected:
		Pixel * colourMap;
		U16 colourMapSize;
		
	};
	
}



#endif	/* CONVERTORCOLOURMAP_H */

