#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>

#include "../../common/types.h"
#include "../../common/Exception.h"

#include "ConvertorTrueColour.h"

using namespace doubrtom;
using namespace std;

ConvertorTrueColour::ConvertorTrueColour(void * buffer, U16 width, U16 height, PixelFormat & format)
: ConvertorBase(buffer, width, height, format) {}

Pixel ConvertorTrueColour::getPixel(void * pointer) {
	Pixel px;
	
	switch (pixelFormat.bitsPerPixel) {
		case 8:
			px = (Pixel) *((U8 *) pointer);
			break;
		case 16:
			if (isBigEndian != pixelFormat.bigEndianFlag) px = (Pixel) __bswap_16(*((U16 *) pointer));
			else px = (Pixel) *((U16 *) pointer);
			break;
		case 32:
			px = (Pixel) *((U32 *) pointer);
			if (isBigEndian != pixelFormat.bigEndianFlag) px = (Pixel) __bswap_32(px);
			break;
		default:
			throw Exception("Unsupported bits per pixel value: %d.", pixelFormat.bitsPerPixel);
	}
	
	return px;
}

void ConvertorTrueColour::setPixel(U8 *& buffer, Pixel px) {
//	*(buffer++) = (px >> (8 + pixelFormat.redShift)) & ((Pixel) pixelFormat.redMax);
//	*(buffer++) = (px >> (8 + pixelFormat.greenShift)) & ((Pixel) pixelFormat.greenMax);
//	*(buffer++) = (px >> (8 + pixelFormat.blueShift)) & ((Pixel) pixelFormat.blueMax);
	*(buffer++) = (px >> pixelFormat.redShift) & (pixelFormat.redMax);
	*(buffer++) = (px >> pixelFormat.greenShift) & (pixelFormat.greenMax);
	*(buffer++) = (px >> pixelFormat.blueShift) & (pixelFormat.blueMax);
}

void ConvertorTrueColour::copyRect(void * buffer) {
	copyRect(this->r, buffer);
}

void ConvertorTrueColour::copyRect(U16 srcX, U16 srcY) {
	U8 * copyBuffer = new U8[r.width * r.height * 3];
	U8 * dstPointer = copyBuffer;
	U8 * srcPointer = this->screenBuffer + (screenWidth*srcY*3) + (srcX*3);
	
	for (int i=0; i<r.height; i++) {
		memcpy(dstPointer, srcPointer, r.width * 3);
		dstPointer += r.width * 3;
		srcPointer += screenWidth * 3;
	}
	
	dstPointer = this->screenBuffer + (screenWidth*r.y*3) + (r.x*3);
	srcPointer = copyBuffer;
	
	for (int i=0; i<r.height; i++) {
		memcpy(dstPointer, srcPointer, r.width * 3);
		dstPointer += screenWidth * 3;
		srcPointer += r.width * 3;
	}

	delete[] copyBuffer;
}

void ConvertorTrueColour::copyRect(Rect & r, void * buffer) {
	U8 * rowSrc = (U8 *) buffer;
	U8 * rowDst = this->screenBuffer + (this->screenWidth*r.y*3) + (r.x*3);
	
	for (int i=0; i < r.height; i++) {
		for (int j=0; j < r.width; j++) {
			Pixel px = getPixel((void *) rowSrc);
			setPixel(rowDst, px);
			
			rowSrc += pixelFormat.bitsPerPixel/8;
		}
		
		rowDst += (this->screenWidth - r.width) * 3;
	}
}

void ConvertorTrueColour::setPixels(Rect & r, Pixel px) {
	U8 * row = this->screenBuffer + (this->screenWidth*r.y*3) + (r.x*3);

	for (int i=0; i<r.height; i++) {
		for (int j=0; j<r.width; j++) {
			setPixel(row, px);
		}
		
		row += (this->screenWidth - r.width) * 3;
	}
}

void ConvertorTrueColour::setPixels(Pixel px) {
	setPixels(this->r, px);
}

