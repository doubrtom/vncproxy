#ifndef CONVERTORTRUECOLOUR_H_876545678765
#define	CONVERTORTRUECOLOUR_H_876545678765

#include "ConvertorBase.h"

#include "../../common/types.h"

namespace doubrtom {
	
	/**
	 * Class for converting different true colour formats into RGB888
	 */
	class ConvertorTrueColour : public ConvertorBase {
	public:
		/**
		 * Constructor
     * @see ConvertorBase::ConvertorBase(void * buffer, U16 width, U16 height, PixelFormat & format)
     */
		ConvertorTrueColour(void * buffer, U16 width, U16 height, PixelFormat & format);
		virtual ~ConvertorTrueColour() {}
		
		virtual void copyRect(void * buffer);
		virtual void copyRect(U16 srcX, U16 srcY);
		virtual void copyRect(Rect & r, void * buffer);
		virtual void setPixels(Rect & r, Pixel px);
		virtual void setPixels(Pixel px);
		
		/**
		 * Return pixel from pointer position.
     * @param pointer		Pointer to image data.
     * @return					Pixel value at pointer position.
     */
		Pixel getPixel(void * pointer);
		/**
		 * Set pixel in buffer to px value.
     * @param buffer		Buffer with image data.
     * @param px				Pixel value
     */
		void setPixel(U8 *& buffer, Pixel px);
		
	protected:
		
		
	};
	
}

#endif	/* CONVERTORTRUECOLOUR_H_876545678765 */

