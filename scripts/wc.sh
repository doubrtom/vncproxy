#!/bin/bash

find ../. -path .././pugixml -prune -o -path .././doc -prune -o -regex '.*\.\(cpp\|h\)' -exec wc -l {} \; | awk 'BEGIN {sum = 0} {sum += $1} END {print "Pocet radku: " sum}'
