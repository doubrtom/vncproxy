#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>

#include "../common/Logger.h"
#include "../common/Exception.h"
#include "Config.h"
#include "Server.h"

using namespace std;
using namespace doubrtom;

Config::Config(Server & s) : server(s) {
	homeDir = getenv("HOME");
	if (homeDir == NULL) throw Exception("Missing environment variable HOME.");

	sprintf(programDir, "%s/.vncproxy", homeDir);
	proxyDisplayNum = 0;
	vncServerDisplayNum = 0;
	recordComm = false;
	screenCapture = false;
	replayingRecord = false;
	imageDir[0] = 0;
	recordFile[0] = 0;
	sprintf(host, "localhost");
	playbackSpeed = 1.0;
}

/* ----------------- methods ---------------------*/

void Config::processParameters(int argc, char** argv) {
	if (argc < 2) throw Exception("Missing mandatory parameter :display.");

	readDisplayNumber(argv[1]);

	for (int i = 1; i < argc-1; i += 2) {
		if (argv[i][0] == '-' && i + 1 < argc) {
			setCLIValue(&(argv[i][1]), argv[i + 1]);
		}
	}
}

void Config::readDisplayNumber(const char * str) {
	const char * errorMsg = "Display number has to be first parameter in format :display";

	if (str[0] != ':') throw Exception(errorMsg);

	int display = atoi(&(str[1]));
	if (display == 0) throw Exception(errorMsg);

	proxyDisplayNum = display;
}

void Config::setCLIValue(const char * key, const char * value) {
	if (!strcmp(key, "server")) setVncServer(value);
	else if (!strcmp(key, "imagedir")) setImageDir(value);
	else if (!strcmp(key, "recordfile")) setRecordFile(value);
	else if (!strcmp(key, "threadpool")) server.setThreadPoolSize(value);
	else throw Exception("Unknown vncproxy parametr: %s", key);
}

void Config::createProgramDir() {
	int result = mkdir(programDir, 0755);
	if (result == -1 && errno != EEXIST) {
		throw Exception("Unable to create vncproxy directory ~/.vncproxy");
	}
}

void Config::readConfigFile() {
	string line, key, value;
	char configFilePath[strlen(programDir) + 12];
	sprintf(configFilePath, "%s/config.txt", programDir);

	ifstream configFile(configFilePath, ios::in);
	if (!configFile.is_open()) return; // if no config file, do nothing

	if (!getline(configFile, line)) {
		configFile.close();
		return;
	}
	if (line.compare("vncproxy")) {
		throw Exception("Config file is corrupted, please repair it or remove it. Path is ~/.vncproxy/config.txt");
	}

	while (getline(configFile, key, '=') && getline(configFile, value)) {
		setCLIValue(key.c_str(), value.c_str());
	}

	configFile.close();
}

void Config::checkConfig() {
	if (proxyDisplayNum == 0) throw Exception("Missing mandatory parameter :display.");
	if (vncServerDisplayNum == 0) vncServerDisplayNum = proxyDisplayNum - 1;
}

/* --------------- setters & getters ------------ */

int Config::getProxyDisplay() const {
	return proxyDisplayNum;
}

int Config::getVncServerDisplay() const {
	return vncServerDisplayNum;
}

int Config::getVncServerPort() const {
	return vncServerDisplayNum + VNC_BASE_PORT;
}

int Config::getProxyPort() const {
	return proxyDisplayNum + VNC_BASE_PORT;
}

double Config::getPlaybackSpeed() const {
	return playbackSpeed;
}

const char * Config::getImageDir() const {
	return imageDir;
}

const char * Config::getRecordFile() const {
	return recordFile;
}

const char * Config::getProgramDir() const {
	return programDir;
}

bool Config::isRecording() const {
	return recordComm;
}

bool Config::isReplaying() const {
	return replayingRecord;
}

bool Config::isScreenCapturing() const {
	return screenCapture;
}

const char * Config::getHost() const {
	return host;
}

void Config::setProxyDisplay(int display) {
	proxyDisplayNum = display;
}

void Config::setProxyDisplay(const char * display) {
	int p = atoi(display);
	if (p == 0) throw Exception("Invalid display for vncproxy.");

	proxyDisplayNum = p;
}

void Config::setVncServer(const char * host, int display) {
	strcpy(this->host, host);
	vncServerDisplayNum = display;
}

void Config::setVncServer(const char* hostDisplay) {
	const char * errorMsg = "Server parameter has to be in format <host>:<display>. Host can be empty for localhost.";
	const char * delimiter = strrchr(hostDisplay, ':');
	if (delimiter == NULL) throw Exception(errorMsg);
  if ((unsigned int) (delimiter-hostDisplay) >= MAX_STRING_LENGTH) throw Exception("Max length for host name is %d.", MAX_STRING_LENGTH);

	if (delimiter == hostDisplay) sprintf(host, "localhost");
	else {
    int len = delimiter - hostDisplay;
		memcpy(host, hostDisplay, len * sizeof (char));
		host[len] = 0;
	}
	int display = atoi(delimiter + 1);

	if (display == 0) throw Exception(errorMsg);
	else this->vncServerDisplayNum = display;
  
  printf("host je %s.\n", this->host);
  printf("port je %d.\n", this->getVncServerPort());
}

void Config::setImageDir(const char * dir) {
	if (strlen(dir) >= MAX_STRING_LENGTH) {
		throw Exception("Maximum lengt of imagedir is %d.", MAX_STRING_LENGTH);
	}
	
	struct stat st;
	if (stat(dir, &st) < 0) throw Exception("Unable to create stat struct for testing directory.");
	if (!S_ISDIR(st.st_mode)) throw Exception("Directory %s doesn't exist.");

        strcpy(imageDir, dir);
}

void Config::setRecordFile(const char * file) {
	if (strlen(file) >= MAX_STRING_LENGTH) {
		throw Exception("Maximum length of recordfile is %d.", MAX_STRING_LENGTH);
	}

	strcpy(recordFile, file);
}

void Config::setRecordReplaying(bool replaying) {
	replayingRecord = replaying;
}

void Config::setCommRecording(bool record) {
	recordComm = record;
}

void Config::setScreenCapturing(bool save) {
	screenCapture = save;
}

void Config::setPlaybackSpeed(const double speed) {
	playbackSpeed = speed;
}

