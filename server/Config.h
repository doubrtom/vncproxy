#ifndef CONFIG_H_8987654546728926
#define	CONFIG_H_8987654546728926

#include <cstdlib>

namespace doubrtom {
	
	class Logger;
	class Server;

	/**
	 * Class containing config of proxy server and VNC server and client.
	 */
	class Config {
	public:

		static const size_t MAX_STRING_LENGTH = 254;
		static const int VNC_BASE_PORT = 5900;

		Config(Server & s);
		/**
		 * Process parameters from CLI.
     * @param argc		Count of program paramaters.
     * @param argv		Program parameters.
     */
		void processParameters(int argc, char ** argv);
		/**
		 * Read display number from CLI
     * @param str
     */
		void readDisplayNumber(const char * str);
		/**
		 * Create program dir - '~/.vncproxy'.
     */
		void createProgramDir();
		/**
		 * Read config file from program dir - '~/.vncproxy/config.txt'.
     */
		void readConfigFile();
		/**
		 * Check if all mandatory parameters was set.
     */
		void checkConfig();

		int getProxyDisplay() const;
		int getProxyPort() const;
		int getVncServerDisplay() const;
		int getVncServerPort() const;
		bool isRecording() const;
		bool isScreenCapturing() const;
		bool isReplaying() const;
		const char * getImageDir() const;
		const char * getRecordFile() const;
		const char * getHost() const;
		const char * getProgramDir() const;
		double getPlaybackSpeed() const;

		/**
		 * Method for saving CLI parameters.
     * @param key			Command	key.
     * @param value		Command value.
     */
		void setCLIValue(const char * key, const char * value = 0);

		void setProxyDisplay(int display);
		void setProxyDisplay(const char * display);
		void setVncServer(const char * host, int display);
		void setVncServer(const char * hostDisplay);
		void setRecordReplaying(bool replaying);
		void setCommRecording(bool record);
		void setScreenCapturing(bool save);
		void setImageDir(const char * dir);
		void setRecordFile(const char * file);
		void setPlaybackSpeed(const double speed);


	protected:
		Server & server;

		const char * homeDir;
		char programDir[MAX_STRING_LENGTH];
		char host[MAX_STRING_LENGTH];
		int proxyDisplayNum; // port for listen
		int vncServerDisplayNum; // port with vncserver
		
		bool recordComm;
		bool replayingRecord;
		bool screenCapture;
		double playbackSpeed;
		
		char imageDir[MAX_STRING_LENGTH];
		char recordFile[MAX_STRING_LENGTH];
	};

}


#endif	/* CONFIG_H */

