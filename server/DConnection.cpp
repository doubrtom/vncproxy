#include <cstdlib>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <netdb.h>
#include <iostream>
#include <sys/select.h>
#include <sys/time.h>
#include <bits/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <bits/errno.h>
#include <netinet/in.h>

#include "DConnection.h"
#include "Server.h"
#include "../common/Exception.h"
#include "../common/types.h"
#include "../common/vncInfo.h"

using namespace std;
using namespace doubrtom;

DConnection::DConnection(Server & s) : server(s), serverToClient(s), clientToServer(s), c(s, "DConnection") {
	supportedSecurityLen = 2;
	supportedSecurity[0] = VncSecurity::NONE;
	supportedSecurity[1] = VncSecurity::VNC_AUTHENTICATION;
	
	on = 1;
	
	memset(&proxyAddr, 0, sizeof (proxyAddr));
	memset(&proxyAddr, 0, sizeof (clientAddr));
	
	proxySocket = INVALID_DESCRIPTOR;
	clientSocket = INVALID_DESCRIPTOR;
	serverSocket = INVALID_DESCRIPTOR;
}

DConnection::~DConnection() {
	if (proxySocket != INVALID_DESCRIPTOR) close(proxySocket);
	if (clientSocket != INVALID_DESCRIPTOR) close(clientSocket);
	if (serverSocket != INVALID_DESCRIPTOR) close(serverSocket);
}

void DConnection::createClientConnection() {
	if ((proxySocket = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (setsockopt(proxySocket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on)) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	int off = 0;
	if (setsockopt(proxySocket, IPPROTO_IPV6, IPV6_V6ONLY, (int *)&off, sizeof(off)) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (fcntl(proxySocket, F_SETFL, O_NONBLOCK) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	proxyAddr.sin6_family = AF_INET6;
	proxyAddr.sin6_port = htons((uint16_t) server.getConfig().getProxyPort());
	proxyAddr.sin6_addr = in6addr_any;
        
	if (bind(proxySocket, (sockaddr *) &proxyAddr, sizeof (proxyAddr)) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (listen(proxySocket, 1) < 0) {
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
}

void DConnection::createServerConnection() {
	memset(&hints, 0, sizeof (hints));
	hints.ai_socktype = SOCK_STREAM;
	
	char port[50];
	sprintf(port, "%d", server.getConfig().getVncServerPort());
	
	int status = getaddrinfo(
					server.getConfig().getHost(),
					port,
					&hints,
					&serverInfo
	);
	
	if (status != 0) {
		freeaddrinfo(serverInfo);
		throw Exception(Exception::EX_DCONN_CONN, gai_strerror(status), status);
	}
	
	if ((serverSocket = socket(serverInfo->ai_family, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		freeaddrinfo(serverInfo);
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on)) < 0) {
		freeaddrinfo(serverInfo);
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (fcntl(serverSocket, F_SETFL, O_NONBLOCK) < 0) {
		freeaddrinfo(serverInfo);
		throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
	}
	
	if (connect(serverSocket, serverInfo->ai_addr, serverInfo->ai_addrlen) < 0) {
		if (errno != EINPROGRESS) {
			freeaddrinfo(serverInfo);
			throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
		}
	}
	
	freeaddrinfo(serverInfo);
}

void DConnection::acceptClient() {
	addrlen = sizeof (clientAddr);
	FD_ZERO(&masterSet);
	FD_SET(proxySocket, &masterSet);
	
	while (server.isRunning()) {
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		workingSet = masterSet;
		
		selected = select(proxySocket+1, &workingSet, NULL, NULL, &timeout);
		
		if (selected < 0) {
			if (errno == EINTR) throw Exception(Exception::EX_EINTR);
			throw Exception(Exception::EX_DCONN_COMM, "select", strerror(errno), errno);
		}
		if (selected == 0) continue;
		
		if (FD_ISSET(proxySocket, &workingSet)) {
			if ((clientSocket = accept(proxySocket, (sockaddr *) &clientAddr, &addrlen)) < 0) {
				throw Exception(Exception::EX_DCONN_CONN, strerror(errno), errno);
			}
			break;
		}
	}
	
	if (!server.isRunning()) throw Exception(Exception::EX_VNCPROXY_CLOSED_BY_USER);
}

void DConnection::startVncCommunication() {
	createClientConnection();
	acceptClient();
	createServerConnection();

	// set socket to server class
	server.setClientSocket(clientSocket);
	server.setServerSocket(serverSocket);
	
	vncHandshaking();
	vncSecurity();
	vncInitMessage();
	
	server.initScreen();
	server.initScreenConvertor();
	
	vncCommunication();
}

void DConnection::serverError() {
	U32 msgLen;
	c.setSockets(serverSocket, clientSocket);
	c.readU32(msgLen);
	
	char * msg = new char[msgLen + 1];
	c.read(msg, msgLen);
	msg[msgLen] = 0;
	
	c.sendU32(msgLen);
	c.send(msg, msgLen);
	c.flushBuffer();
	
        Exception ex(Exception::EX_VNC_HAND_SHAKING, msg);
	delete[] msg;
	
	throw ex;
}

void DConnection::filterSecurityType(U8 & securityLen, U8 ** security) {
	U8 newLen = 0;
	for (U8 i=0; i<securityLen; i++) {
		for (U8 j=0; j<supportedSecurityLen; j++) {
			if ((*security)[i] == supportedSecurity[j]) {
				newLen++;
				break;
			}
		}
	}
	
	U8 * newSecurity = new U8[newLen];
	int pos = 0;
	
	for (U8 i=0; i<securityLen; i++) {
		for (U8 j=0; j<supportedSecurityLen; j++) {
			if ((*security)[i] == supportedSecurity[j]) {
				newSecurity[pos++] = (*security)[i];
				break;
			}
		}
	}
	
	delete[] *security;
	*security = newSecurity;
	securityLen = newLen;
}

void DConnection::vncSecuritySpecificData(U8 selected) {
	if (selected == VncSecurity::NONE) return;
	if (selected == VncSecurity::VNC_AUTHENTICATION) {
		// resend chalenge
		c.setSockets(serverSocket, clientSocket);
		c.forward(16);
		c.flushBuffer();
		
		// resend response
		c.setSockets(clientSocket, serverSocket);
		c.forward(16);
		c.flushBuffer();
	}
}

void DConnection::vncHandshaking() {
	char protocolVersion[13];
	
	c.setSockets(serverSocket, clientSocket);
	c.forward(protocolVersion, 12);
	c.flushBuffer();
	protocolVersion[12] = 0;
	
	c.setSockets(clientSocket, serverSocket);
	c.forward(protocolVersion, 12);
	c.flushBuffer();
	protocolVersion[12] = 0;
}

void DConnection::vncSecurity() {
	U8 securityLen;
	U8 selected;
	
	c.setSockets(serverSocket, clientSocket);
	c.readU8(securityLen);
	if (securityLen == 0) serverError();
	
	U8 * security = new U8[securityLen];
	
	try {
		c.read(security, securityLen);

		filterSecurityType(securityLen, &security);

		c.setSockets(serverSocket, clientSocket);
		c.sendU8(securityLen);
		c.send(security, securityLen);
		c.flushBuffer();
	} catch (Exception & ex) {
		delete[] security;
		throw ex;
	}
	
	delete[] security;
	
	// resend selected security
	c.setSockets(clientSocket, serverSocket);
	c.forwardU8(selected);
	c.flushBuffer();
	
	vncSecuritySpecificData(selected);
	
	// resend security result
	U32 result;
	c.setSockets(serverSocket, clientSocket);
	c.forwardU32(result);
	c.flushBuffer();
	
	if (result == 1) serverError();
}

void DConnection::vncServerInitMessage() {
	VncServerInfo & info = server.getVncServerInfo();
	
	c.setSockets(serverSocket, clientSocket);
	
	c.forwardU16(info.framebufferWidth);
	c.forwardU16(info.framebufferHeight);
	
	
	c.forwardU8(info.pixelFormat.bitsPerPixel);
	c.forwardU8(info.pixelFormat.depth);
	c.forwardU8(info.pixelFormat.bigEndianFlag);
	c.forwardU8(info.pixelFormat.trueColourFlag);
	c.forwardU16(info.pixelFormat.redMax);
	c.forwardU16(info.pixelFormat.greenMax);
	c.forwardU16(info.pixelFormat.blueMax);
	c.forwardU8(info.pixelFormat.redShift);
	c.forwardU8(info.pixelFormat.greenShift);
	c.forwardU8(info.pixelFormat.blueShift);
	
	c.forward(3);
	
	c.forwardU32(info.nameLength);
	
	if (info.name != NULL) delete[] info.name; // for re-connect of client and server
	info.name = new U8[info.nameLength + 1];
	
	c.forward(info.name, info.nameLength);
	c.flushBuffer();
	
	info.name[info.nameLength] = 0;
	
	// init screen
	server.initScreen();
}

void DConnection::vncInitMessage() {
	// ClientInit message
	U8 init;
	c.setSockets(clientSocket, serverSocket);
	c.forwardU8(init);
	c.flushBuffer();
	
	// ServerInit message
	vncServerInitMessage();
}

void DConnection::vncCommunication() {
	FD_ZERO(&masterSet);
	FD_SET(serverSocket, &masterSet);
	FD_SET(clientSocket, &masterSet);
	
	int max = (clientSocket > serverSocket) ? clientSocket : serverSocket;
	maxSocket = max;
	int ready;
	
	serverToClient.setSockets(clientSocket, serverSocket);
	clientToServer.setSockets(clientSocket, serverSocket);
	
	server.setConnected(true);
	
	while (server.isRunning()) {
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;
		workingSet = masterSet;
		
		selected = select(max+1, &workingSet, NULL, NULL, &timeout);
		
		if (selected < 0) {
			if (errno == EINTR) throw Exception(Exception::EX_EINTR);
			throw Exception(Exception::EX_DCONN_COMM, "select", strerror(errno), errno);
		}
		if (selected == 0) continue;
		
		ready = selected;
		for (int i=0; i <= maxSocket && ready > 0; i++) {
			if (FD_ISSET(i, &workingSet)) {
				ready--;
				
				if (i == serverSocket) {
					serverToClient.processMessage();
				} else if (i == clientSocket) {
					clientToServer.processMessage();
				} else {
					throw Exception("Unknow socket.");
				}
			}
		}
	}
	
}