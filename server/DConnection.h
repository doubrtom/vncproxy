#ifndef DCONNECTION_H_9876545678
#define	DCONNECTION_H_9876545678

#include <netinet/in.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>

#include "../common/Communicator.h"
#include "../common/types.h"
#include "VncServerToClient.h"
#include "VncClientToServer.h"

namespace doubrtom{

	class Server;

	/**
	 * Class for data communication between VNC server and client.
	 */
	class DConnection {
	public:
		DConnection(Server & s);
		virtual ~DConnection();
		
		/**
		 * Method for create socket for connection to VNC client.
     */
		void createClientConnection();
		/**
		 * Method for create connection to VNC server.
     */
		void createServerConnection();
		/**
		 * Method for accepting connection from VNC client.
     */
		void acceptClient();
		/**
		 * Start VNC communication - proccess 3 basic steps in beggining of VNC communication.
     */
		void startVncCommunication();
		/**
		 * Read error from VNC server
     */
		void serverError();
		/**
		 * Filter VNC security into security supported by proxy server.
     * @param securityLen		Security len. In and Out parameter.
     * @param security			List of securities. In and Out parameter.
     */
		void filterSecurityType(U8 & securityLen, U8 ** security);

		/**
		 * Procces first step of beggining of VNC communication - handshaking.
     */
		void vncHandshaking();
		/**
		 * Process specific data of VNC security.
     * @param selected		Selected security.
     */
		void vncSecuritySpecificData(U8 selected);
		/**
		 * Process second step of beggining of VNC communicaiton - security.
     */
		void vncSecurity();
		/**
		 * Process third step of beggining of VNC communicaiton - init messages - server part.
     */
		void vncServerInitMessage();
		/**
		 * Process third step of beggining of VNC communicaiton - init messages - client part.
     */
		void vncInitMessage();
		/**
		 * Process communication of VNC server and client.
     */
		void vncCommunication();

	protected:
		U8 supportedSecurity[2];
		int supportedSecurityLen;

		Server & server;
		VncServerToClient serverToClient;
		VncClientToServer clientToServer;
		Communicator c;

		int clientSocket, proxySocket, serverSocket;
		sockaddr_in6 proxyAddr, clientAddr;
		socklen_t addrlen;

		addrinfo hints;
		addrinfo * serverInfo;

		// for main communication phase
		fd_set masterSet, workingSet;
		int maxSocket;
		int selected;
		timeval timeout;
		
		int on;
		
		static const int INVALID_DESCRIPTOR = -2;
	};

}

#endif	/* DCONNECTION_H */

