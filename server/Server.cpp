#include <X11/Xlib.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <X11/extensions/XI.h>
#include <unistd.h>

#include "Server.h"
#include "../record/RecordPlayer.h"
#include "../common/Exception.h"

#include "../screen/Screen.h"
#include "../screen/convertor/ConvertorTrueColour.h"
#include "../screen/convertor/ConvertorBase.h"

using namespace doubrtom;
using namespace std;

Server::Server() : logger(), config(*this), recordPlayer(*this), screenCapture(*this) {
	running = true;
	connected = false;

	clientSocket = 0;
	serverSocket = 0;

	convertor = NULL;

	pthread_mutex_init(&toServerMutex, NULL);

	setCpuCoresCount();
	threadPoolSize = cpuCoresCount + 1;
}

Server::~Server() {
	if (convertor != NULL) delete convertor;
}

void Server::reinit() {
	pthread_mutex_unlock(&toServerMutex);

	clientSocket = 0;
	serverSocket = 0;

	if (convertor != NULL) delete convertor;
	convertor = NULL;

	running = true;
	connected = false;
}

/* ------- synchronization --------------------- */

void Server::lockToServerComm() {
	pthread_mutex_lock(&toServerMutex);
}

void Server::unlockToServerComm() {
	pthread_mutex_unlock(&toServerMutex);
}

/* ------ setters and getters ----------------- */

Logger & Server::getLogger() {
	return logger;
}

Config & Server::getConfig() {
	return config;
}

VncServerInfo & Server::getVncServerInfo() {
	return vncServerinfo;
}

RecordWriter & Server::getRecordWriter() {
	return recordWriter;
}

RecordPlayer & Server::getRecordPlayer() {
	return recordPlayer;
}

doubrtom::Screen & Server::getScreen() {
	return screen;
}

ScreenCapture & Server::getScreenCapture() {
	return screenCapture;
}

void Server::stopServer() {
	running = false;
}

void Server::setClientSocket(int socket) {
	clientSocket = socket;
}

void Server::setServerSocket(int socket) {
	serverSocket = socket;
}

int Server::getBytesPerPixel() {
	return ((int) (vncServerinfo.pixelFormat.bitsPerPixel / 8));
}

bool Server::isRunning() {
	return running;
}

int Server::getClientSocket() {
	return clientSocket;
}

int Server::getServerSocket() {
	return serverSocket;
}

ConvertorBase * Server::getConvertor() {
	return convertor;
}

int Server::getCpuCoresCount() {
	return cpuCoresCount;
}

void Server::setCpuCoresCount(int count) {
	cpuCoresCount = count;
}

void Server::setCpuCoresCount() {
	cpuCoresCount = sysconf(_SC_NPROCESSORS_ONLN);
	if (cpuCoresCount < 1) cpuCoresCount = 1;
}

int Server::getThreadPoolSize() {
	return threadPoolSize;
}

void Server::setThreadPoolSize(int size) {
	this->threadPoolSize = size;
}

void Server::setThreadPoolSize(const char * size) {
	int s = atoi(size);
	if (size <= 0) throw Exception("Unsupported threadpool size.");
	setThreadPoolSize(s);
}

bool Server::isConnected() {
	return connected;
}

void Server::setConnected(bool value) {
	connected = value;
}

/* ---- REPLAY THREAD --------- */

void createThread(Server * server) {
	try {
		server->getRecordPlayer().run();
	} catch (Exception & ex) {
		cerr << ex.getMessage() << endl;
	}

	server->getConfig().setRecordReplaying(false);
}

void Server::startReplayThread() {
	int id;
	recordPlayer.getCommunicator().setSockets(clientSocket, serverSocket);

	id = pthread_create(&replayThread, NULL, (void * (*)(void *)) createThread, (void *) this);
	int detach = pthread_detach(replayThread);
	if (detach != 0) throw Exception("Cannot detach replay thread: %s.", strerror(detach));
	if (id != 0) throw Exception("Cannot create replay thread: %s", strerror(id));
}

void Server::stopReplayThread() {
	recordPlayer.stop();
}

/* ------ SCREEN CAPTURE PART ------------ */

void Server::initScreen() {
	screen.setScreen(vncServerinfo.framebufferWidth, vncServerinfo.framebufferHeight);
	if (convertor) convertor->setScreenBuffer(screen.buffer);
}

void Server::initScreenConvertor() {
	if (convertor != NULL) {
		delete convertor;
		convertor = NULL;
	}

	if (vncServerinfo.pixelFormat.trueColourFlag) {
		convertor = new ConvertorTrueColour(screen.buffer, vncServerinfo.framebufferWidth, vncServerinfo.framebufferHeight, vncServerinfo.pixelFormat);
	} else {
		convertor = new ConvertorColourMap(screen.buffer, vncServerinfo.framebufferWidth, vncServerinfo.framebufferHeight, vncServerinfo.pixelFormat);
	}
}
