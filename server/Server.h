#ifndef SERVER_H_8765456789
#define	SERVER_H_8765456789

#include <pthread.h>

#include "../common/Logger.h"
#include "Config.h"
#include "../common/vncInfo.h"
#include "../common/types.h"
#include "../record/RecordPlayer.h"
#include "../record/RecordWriter.h"
#include "../screen/Screen.h"
#include "../screen/ScreenCapture.h"
#include "../screen/convertor/ConvertorColourMap.h"

namespace doubrtom {
	
	class ConvertorBase;
	
	/**
	 * Class represent proxy server. Main class of whole project connecting together different parts.
   */
	class Server {
	public:
		Server();
		~Server();
		
		/**
		 * Reinit proxy server for connection of new VNC client and server.
     */
		void reinit();

		// Getters
		Logger & getLogger();
		Config & getConfig();
		VncServerInfo & getVncServerInfo();
		RecordWriter & getRecordWriter();
		RecordPlayer & getRecordPlayer();
		doubrtom::Screen & getScreen();
		ScreenCapture & getScreenCapture();
		ConvertorBase * getConvertor();
		
		int getBytesPerPixel();
		int getCpuCoresCount();
		void setCpuCoresCount(int count);
		void setCpuCoresCount();
		int getThreadPoolSize();
		void setThreadPoolSize(int size);
		void setThreadPoolSize(const char * size);
		bool isConnected();
		void setConnected(bool value);
		
		/**
		 * Stop proxy server.
     */
		void stopServer();
		/**
		 * Create new thread for "playback" mode
     */
		void startReplayThread();
		/**
		 * Stop "playback" mode and thread created for it.
     */
		void stopReplayThread();
		
		/**
		 * Init Screen class with framebuffer according to pixel format.
     */
		void initScreen();
		/**
		 * Init ScreenConvertor according to pixel format.
     */
		void initScreenConvertor();
		
		/**
		 * Lock mutex for messages to VNC server.
     */
		void lockToServerComm();
		/**
		 * Unlock mutex for messages to VNC server.
     */
		void unlockToServerComm();
		
		// settrs and getters
		void setClientSocket(int socket);
		void setServerSocket(int socket);
		
		bool isRunning();
		int getClientSocket();
		int getServerSocket();
		
		// set DEV or PROD mode
		// DEV mode print info into command line
		static const bool DEV_MODE = false;
	protected:
		Logger logger;
		Config config;
		VncServerInfo vncServerinfo;
		RecordWriter recordWriter;
		RecordPlayer recordPlayer;
		
		doubrtom::Screen screen;
		ScreenCapture screenCapture;
		ConvertorBase * convertor;
		
		int clientSocket;
		int serverSocket;
		
		int cpuCoresCount;
		int threadPoolSize;
		
		pthread_t replayThread;
		pthread_mutex_t toServerMutex;
		
		volatile bool running;
		bool connected;
	};
	
}

#endif	/* SERVER_H */

