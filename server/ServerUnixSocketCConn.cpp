#include <cstdlib>
#include <errno.h>
#include <error.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <fstream>
#include <sstream>

#include "../common/UnixSocketCConn.h"
#include "ServerUnixSocketCConn.h"
#include "Server.h"
#include "../common/Exception.h"

#include <iostream>
#include <sys/select.h> // TESTING

using namespace std;
using namespace doubrtom;

ServerUnixSocketCConn::ServerUnixSocketCConn(Server & s) : server(s) {
	socketPath = new char[strlen(server.getConfig().getProgramDir()) + 8];
	sprintf(socketPath, "%s/socket", server.getConfig().getProgramDir());

	responseCode = 0;
	on = 1;
}

ServerUnixSocketCConn::~ServerUnixSocketCConn() {
	delete[] socketPath;

	// clean up all sockets
	for (int i = 0; i <= maxSocket; i++) {
		if (FD_ISSET(i, &masterSet)) close(i);
	}
}

void ServerUnixSocketCConn::createSocket() {
	const char * errorMsg = "Unable to create control connection: %s";

	if ((serverSocket = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		throw Exception(errorMsg, strerror(errno));
	}

	if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof (on)) < 0) {
		throw Exception(errorMsg, strerror(errno));
	}

	// set non-blocking socket
	if (ioctl(serverSocket, FIONBIO, (char *) &on) < 0) {
		throw Exception(errorMsg, strerror(errno));
	}

	serverAddr.sun_family = AF_UNIX;
	strcpy(serverAddr.sun_path, socketPath);
	unlink(serverAddr.sun_path);
	int result = bind(serverSocket, (sockaddr*) & serverAddr,
					strlen(serverAddr.sun_path) + sizeof (serverAddr.sun_family)
					);
	if (result < 0) {
		throw Exception(errorMsg, strerror(errno));
	}
	listen(serverSocket, 10); // TODO zvysit ?

	// init the master fd_set
	FD_ZERO(&masterSet);
	FD_ZERO(&workingSet);
	FD_SET(serverSocket, &masterSet);
	maxSocket = serverSocket;
}

bool ServerUnixSocketCConn::readHeader() {
	int bytesRead = 0, result = 0;

	while (bytesRead < 4) {
		//		result = read(clientSocket, commandHeader + bytesRead, sizeof (commandHeader) - bytesRead);
		result = recv(clientSocket, commandHeader + bytesRead, sizeof (commandHeader) - bytesRead, 0);
		if (result < 0) {
			if (errno == EWOULDBLOCK || errno == EAGAIN) return false;
			close(clientSocket);
			FD_CLR(clientSocket, &masterSet);
			throw Exception("Error during controll connection: %s", strerror(errno));
		}
		if (result == 0) {
			closeConn = true;
			return false; // EOF
		}
		bytesRead += result;
	}

	return true;
}

bool ServerUnixSocketCConn::readArg() {
	int bytesRead = 0, result = 0;

	while (bytesRead < commandHeader[1]) {
		result = read(clientSocket, arg + bytesRead, sizeof (arg) - bytesRead);
		if (result < 0) {
			if (errno == EWOULDBLOCK || errno == EAGAIN) return false;
			close(clientSocket);
			FD_CLR(clientSocket, &masterSet);
			throw Exception("Error during controll connection: %s", strerror(errno));
		}
		if (result == 0) {
			closeConn = true;
			return false;
		}
		bytesRead += result;
	}

	arg[commandHeader[1]] = 0;

	return true;
}

void ServerUnixSocketCConn::processCommand() {
	switch (commandHeader[0]) {
		case START_RECORD:
			commandStartRecord();
			break;
		case STOP_RECORD:
			commandStopRecord();
			break;
		case START_PLAYBACK:
			commandStartPlayback();
			break;
		case STOP_PLAYBACK:
			commandStopPlayback();
			break;
		case SHOW_STATUS:
			commandShowStatus();
			break;
		case SET_RECORD_FILE:
			commandSetRecordFile();
			break;
		case PLAYBACK_SPEED:
			commandPlaybackSpeed();
			break;
		case SET_IMAGE_DIR:
			commandSetImageDir();
			break;
		case START_SCREEN_CAPTURE:
			commandStartScreenCapture();
			break;
		case STOP_SCREEN_CAPTURE:
			commandStopScreenCapture();
			break;
		default:
			responseCode = RESPONSE_UNKNOWN_CMD;
			break;
	}
}

void ServerUnixSocketCConn::processCommands() {
	//	int len = sizeof (sockaddr_un); // REMOVE
	//	clientSocket = accept(serverSocket, NULL, NULL);
	//	if (clientSocket < 0) {
	//		if (errno == EWOULDBLOCK) false;
	//		throw Exception("Control connection failed: %s", strerror(errno));
	//	}

	bool process = true;

	while (process) {
		process = readHeader();
		if (process && commandHeader[1] > 0) process = readArg();
		if (process) {
			processCommand();
			sendResponse();
		}
	}

}

void ServerUnixSocketCConn::sendResponse() {
	commandHeader[0] = responseCode;
	commandHeader[1] = responseMsg.length();

	if (send(clientSocket, commandHeader, sizeof (commandHeader), 0) == -1) {
		throw Exception("Error sending command: %s", strerror(errno));
	}

	if (commandHeader[1] > 0) {
		if (send(clientSocket, responseMsg.c_str(), commandHeader[1], 0) == -1) {
			throw Exception("Error sending command: %s", strerror(errno));
		}

		responseMsg = "";
	}
}

bool ServerUnixSocketCConn::acceptConnections() {
	clientSocket = accept(serverSocket, NULL, NULL);
	if (clientSocket < 0) {
		if (errno == EWOULDBLOCK || errno == EAGAIN) return false;
		throw Exception("Control connection failed: %s", strerror(errno));
	}

	FD_SET(clientSocket, &masterSet);
	if (clientSocket > maxSocket) maxSocket = clientSocket;

	if (clientSocket != -1) return true;
	else return false;
}

void ServerUnixSocketCConn::closeConnection() {
	close(clientSocket);
	FD_CLR(clientSocket, &masterSet);
	if (clientSocket == maxSocket) {
		while (FD_ISSET(maxSocket, &masterSet) == false) maxSocket--;
	}
}

void ServerUnixSocketCConn::processControlCommunication() {
	int descReady;

	while (server.isRunning()) {
		// copy the master fdset over to the working fd_set
		workingSet = masterSet;

		// init timeval struct
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		descReady = select(maxSocket + 1, &workingSet, NULL, NULL, &timeout);

		// fail
		if (descReady < 0) {
			throw Exception("Control connection failed: %s", strerror(errno));
		}

		// timeout
		if (descReady == 0) {
			if (server.isRunning()) continue;
			else break;
		}

		// 1+ descriptors are ready
		for (int i = 0; i <= maxSocket && descReady > 0; i++) {
			if (FD_ISSET(i, &workingSet)) {
				descReady--;

				if (i == serverSocket) {
					while (acceptConnections()) {
					}
				} else {
					clientSocket = i;
					closeConn = false;
					processCommands();
					if (closeConn) closeConnection();
				}
			}
		}

	}
}

/* ------ COMMANDS METHODS - START -------------- */

void ServerUnixSocketCConn::commandPlaybackSpeed() {
	double speed = atof(arg);
	if (speed <= 0) {
		responseCode = RESPONSE_INVALID_ARGUMENT;
		return;
	}

	server.getConfig().setPlaybackSpeed(speed);

	server.getLogger().log("Playback speed set to %lf.", speed);
	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandStartRecord() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (server.getConfig().isRecording()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}
	
	if (server.getConfig().isReplaying()) {
		responseCode = RESPONSE_FAIL;
		responseMsg = "Server is in replaying mode.";
		return;
	}

	const char * filePath = server.getConfig().getRecordFile();

	if (filePath[0] == 0) {
		responseCode = RESPONSE_FAIL;
		responseMsg = "Record file does not set.";
		return;
	}

	if (!server.getRecordWriter().isReady()) {
		try {
			server.getRecordWriter().setRecordFile(filePath);
		} catch (Exception & ex) {
			responseCode = RESPONSE_FAIL;
			responseMsg = ex.getMessage();
			return;
		}
	}

	server.getRecordWriter().start();
	server.getConfig().setCommRecording(true);
	server.getLogger().log("Start communication recording.");
	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandStopRecord() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (!server.getConfig().isRecording()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}
	
	server.getConfig().setCommRecording(false);
	server.getRecordWriter().stop();
	server.getLogger().log("Stop communication recording.");
	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandStartPlayback() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (server.getConfig().isReplaying()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}
	
	if (server.getConfig().isRecording()) {
		responseCode = RESPONSE_FAIL;
		responseMsg = "Server is in recording mode.";
		return;
	}

	const char * filePath = server.getConfig().getRecordFile();

	if (filePath[0] == 0) {
		responseCode = RESPONSE_FAIL;
		responseMsg = "Record file does not set.";
		return;
	}

	try {
		if (!server.getRecordWriter().isReady()) {
			server.getRecordPlayer().getRecordReader().setRecordFile(filePath);
		}

		server.getConfig().setRecordReplaying(true);
		server.startReplayThread();
	} catch (Exception & ex) {
		server.getConfig().setRecordReplaying(false);

		responseCode = RESPONSE_FAIL;
		responseMsg = ex.getMessage();
		return;
	}

	server.getLogger().log("Start record playback.");
	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandStopPlayback() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (!server.getConfig().isReplaying()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}
	
	server.getConfig().setRecordReplaying(false);
	server.stopReplayThread();
	server.getLogger().log("Stop record playback.");
	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandShowStatus() {
	responseCode = RESPONSE_OK;

	Config & config = server.getConfig();
	const char * rf = config.getRecordFile();
	const char * sd = config.getImageDir();

	stringstream ss;

	ss << "VNC Server: " << config.getHost() << ":"
					<< config.getVncServerDisplay() << "\n"
					<< "\tRecording communication: " << boolToStr(config.isRecording())
					<< "\n" << "\tRecord playback: " << boolToStr(config.isReplaying())
					<< "\n" << "\tCapture screen: " << boolToStr(config.isScreenCapturing())
					<< "\n" << "\tRecord file: " << (rf[0] == 0 ? "NOT SET" : rf)
					<< "\n" << "\tImage dir: " << (sd[0] == 0 ? "NOT SET" : sd)
					<< "\n" << "\tPlayback speed: " << config.getPlaybackSpeed()
					<< "\n" << "\tThread pool size: " << server.getThreadPoolSize()
					<< "\n";

	responseMsg = ss.str();
}

void ServerUnixSocketCConn::commandSetRecordFile() {
	try {
		server.getConfig().setRecordFile(arg);

		server.getLogger().log("Record file set to %s", arg);
		responseCode = RESPONSE_OK;
	} catch (Exception & ex) {
		responseCode = RESPONSE_FAIL;
		responseMsg = ex.getMessage();
	}
}

void ServerUnixSocketCConn::commandSetImageDir() {
	try {
		server.getConfig().setImageDir(arg);

		server.getLogger().log("Image dir set to %s", arg);
		responseCode = RESPONSE_OK;
	} catch (Exception & ex) {
		responseCode = RESPONSE_FAIL;
		responseMsg = ex.getMessage();
	}
}

void ServerUnixSocketCConn::commandStartScreenCapture() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (server.getConfig().isScreenCapturing()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}
	
	const char * imageDir = server.getConfig().getImageDir();

	if (imageDir[0] == 0) {
		responseCode = RESPONSE_FAIL;
		responseMsg = "Image dir does not set.";
		return;
	}
	
	server.getLogger().log("Start screen capturing.");
	
	// TODO MUTEX ????? -> vypada bezpecne, nejdriv inicializuje a pak dava tasky
	try {
		server.getScreenCapture().start();
	}	catch (Exception & ex) {
		responseCode = RESPONSE_FAIL;
		responseMsg = ex.getMessage();
		
		server.getLogger().log("Screen capture: %s.", ex.getMessage());
		server.getLogger().log("Stop screen capturing.");
		
		return;
	}
	
	server.getConfig().setScreenCapturing(true);

	responseCode = RESPONSE_OK;
}

void ServerUnixSocketCConn::commandStopScreenCapture() {
	if (!server.isConnected()) {
		responseCode = RESPONSE_NOT_CONNECTED;
		return;
	}
	
	if (!server.getConfig().isScreenCapturing()) {
		responseCode = RESPONSE_ALREADY_SET;
		return;
	}

	// TODO MUTEX ????? -> nejspis ne, maximalne vlozi task pri vypnutem screenCapture, ale novy init vycisti frontu
	server.getConfig().setScreenCapturing(false);
	server.getScreenCapture().stop();

	server.getLogger().log("Stop screen capturing.");
	responseCode = RESPONSE_OK;
}


/* ------ COMMANDS METHODS - END -------------- */

/* ---------- PROTECTED ----------------- */

const char * ServerUnixSocketCConn::boolToStr(bool b) {
	return (b) ? "YES" : "NO";
}