#ifndef SERVERUNIXSOCKETCCONN_H_7654345678
#define	SERVERUNIXSOCKETCCONN_H_7654345678

#include <string>
#include <sys/socket.h>
#include <sys/un.h>

#include "../common/UnixSocketCConn.h"

namespace doubrtom {

	class Server;
	class Exception;

	/**
	 * Class representing server part of control communication.
   */
	class ServerUnixSocketCConn : public UnixSocketCConn {
	public:

		ServerUnixSocketCConn(Server & s);
		virtual ~ServerUnixSocketCConn();

		/**
		 * Create socket for accepting control program.
     */
		void createSocket();

		/**
		 * Read header of control communication.
     * @return	Return false if there is no command from control
     */
		bool readHeader();
		/**
		 * Read command argument.
     * @return Return false if there is no command from control
     */
		bool readArg();
		/**
		 * Proccess connection to control program and start of control communication.
     */
		void processControlCommunication();
		/**
		 * Process one control command.
     */
		void processCommand();
		/**
		 * Process all control commands from connected control program.
     */
		void processCommands();
		/**
		 * Accept connection from control program.
     * @return 
     */
		bool acceptConnections();
		/**
		 * Close connection with control program.
     */
		void closeConnection();
		/**
		 * Send result of processed control command.
     */
		void sendResponse();
		
		// commands
		
		/**
		 * Command for start record mode.
     */
		void commandStartRecord();
		/**
		 * Command for stop record mode.
     */
		void commandStopRecord();
		/**
		 * Command for send proxy server status into control pgoram.
     */
		void commandShowStatus();
		/**
		 * Command for setting record file
     */
		void commandSetRecordFile();
		/**
		 * Command for start playback mode.
     */
		void commandStartPlayback();
		/**
		 * Command for stop playback mode.
     */
		void commandStopPlayback();
		/**
		 * Command for modification playback speed.
     */
		void commandPlaybackSpeed();
		/**
		 * Command for setting image dir.
     */
		void commandSetImageDir();
		/**
		 * Command for start screen capture mode.
     */
		void commandStartScreenCapture();
		/**
		 * Command for stop screen capture mode.
     */
		void commandStopScreenCapture();
		
	protected:
		sockaddr_un clientAddr;
		int clientSocket, maxSocket, newSocket;
		fd_set masterSet, workingSet;
		timeval timeout;
		bool closeConn;

		int on;
		
		U16 responseCode;
		std::string responseMsg;
		
		Server & server;
		
		/**
		 * Convert bool value into string "YES" or "NO".
     * @param b		Value for convert
     * @return		Converted string.
     */
		const char * boolToStr(bool b);
	};

}



#endif	/* SERVERUNIXSOCKETCCONN_H */

