#include <iostream>
#include <cstring>
#include <netinet/in.h>

#include "VncClientToServer.h"
#include "../common/vncInfo.h"
#include "Server.h"
#include "../common/types.h"
#include "../common/Exception.h"
#include "VncServerToClient.h"
#include "../common/Cursor.h"
#include "../screen/Screen.h"


#include "../screen/ScreenSaver.h" // DELETE ?

using namespace std;
using namespace doubrtom;

VncClientToServer::VncClientToServer(Server & s) : server(s), c(s, "VncClientToServer") {
	setSupportedEncodings();
}

VncClientToServer::~VncClientToServer() {
}

void VncClientToServer::setSupportedEncodings() {
	supportedEncodings[0] = VncServerToClient::RAW;
	supportedEncodings[1] = VncServerToClient::COPY_RECT;
	supportedEncodings[2] = VncServerToClient::RRE;
	supportedEncodings[3] = VncServerToClient::HEXTILE;
//	supportedEncodings[4] = VncServerToClient::ZRLE; // not supported in this version
	supportedEncodings[4] = VncServerToClient::CURSOR;
	supportedEncodings[5] = VncServerToClient::DESKTOP_SIZE;
	
////	supportedEncodings[0] = VncServerToClient::RAW;
//	supportedEncodings[0] = VncServerToClient::COPY_RECT;
////	supportedEncodings[1] = VncServerToClient::RRE;
//	supportedEncodings[1] = VncServerToClient::HEXTILE;
////	supportedEncodings[1] = VncServerToClient::ZRLE; // not supported in this version
//	supportedEncodings[2] = VncServerToClient::CURSOR;
//	supportedEncodings[3] = VncServerToClient::DESKTOP_SIZE;
}

void VncClientToServer::setSockets(int clientSocket, int serverSocket) {
	c.setSockets(clientSocket, serverSocket);
}

bool VncClientToServer::isBlockWhileReplaying(U8 msgType) {
	return (msgType == KEY_EVENT || msgType == POINTER_EVENT || msgType == CLIENT_CUT_TEXT);
}

void VncClientToServer::processMessage() {
	do {
		// synchronization
		server.lockToServerComm();

		U8 msgType;
		c.readU8(msgType);

		if (server.getConfig().isReplaying() && isBlockWhileReplaying(msgType)) {
		} else {
			c.sendU8(msgType);
		}

		switch (msgType) {
			case SET_PIXEL_FORMAT:
				setPixelFormat();
				break;
			case SET_ENCODINGS:
				setEncodings();
				break;
			case FRAMEBUFFER_UPDATE_REQUEST:
				framebufferUpdateRequest();
				break;
			case KEY_EVENT:
				keyEvent();
				break;
			case POINTER_EVENT:
				pointerEvent();
				break;
			case CLIENT_CUT_TEXT:
				clientCutText();
				break;
			default:
				server.unlockToServerComm();
				throw Exception("Unsupported client to server message type - %d", (int) msgType);
		} 


		c.flushBuffer();

		// synchronization
		server.unlockToServerComm();
	} while (!c.isEmptyIn());
}

void VncClientToServer::setPixelFormat() {
	VncServerInfo & info = server.getVncServerInfo();
	
	c.skip(3);
	
	c.forwardU8(info.pixelFormat.bitsPerPixel);
	c.forwardU8(info.pixelFormat.depth);
	c.forwardU8(info.pixelFormat.bigEndianFlag);
	c.forwardU8(info.pixelFormat.trueColourFlag);
	c.forwardU16(info.pixelFormat.redMax);
	c.forwardU16(info.pixelFormat.greenMax);
	c.forwardU16(info.pixelFormat.blueMax);
	c.forwardU8(info.pixelFormat.redShift);
	c.forwardU8(info.pixelFormat.greenShift);
	c.forwardU8(info.pixelFormat.blueShift);
	
	if (Server::DEV_MODE) {
		printf("----------------------------- PIXEL FORMAT --------------------------\n");
		printf("Bits per pixel: %d\n", (int) info.pixelFormat.bitsPerPixel);
		printf("depth: %d\n", (int) info.pixelFormat.depth);
		printf("big endian flag: %d\n", (int) info.pixelFormat.bigEndianFlag);
		printf("true colour flag: %d\n", (int) info.pixelFormat.trueColourFlag);
		printf("red max: %d\n", (int) info.pixelFormat.redMax);
		printf("green max: %d\n", (int) info.pixelFormat.greenMax);
		printf("blue max: %d\n", (int) info.pixelFormat.blueMax);
		printf("red shift: %d\n", (int) info.pixelFormat.redShift);
		printf("green shift: %d\n", (int) info.pixelFormat.greenShift);
		printf("blue shift: %d\n", (int) info.pixelFormat.blueShift);
		printf("---------------------------------------------------------------------\n");
	}
	
	
	c.skip(3);
	
	// re-init screen
	server.initScreenConvertor();
}

void VncClientToServer::filterEncodings(U16 & count, S32 ** encodings) {
	U16 newCount = 0;
	
	for (U16 i=0; i<count; i++) {
		for (int j=0; j<SUPPORTED_ENCODINGS_LEN; j++) {
			if (supportedEncodings[j] == (*encodings)[i]) {
				newCount++;
				break;
			}
		}
	}
	
	S32 * newEncodings = new S32[newCount];
	U16 index = 0;
	
	for (U16 i=0; i<count; i++) {
		for (int j=0; j<SUPPORTED_ENCODINGS_LEN; j++) {
			if (supportedEncodings[j] == (*encodings)[i]) {
				newEncodings[index++] = (*encodings)[i];
				break;
			}
		}
	}
	
	delete[] *encodings;
	*encodings = newEncodings;
	count = newCount;
	
	if (count == 0) throw Exception("No supported encoding type.");
}

void VncClientToServer::setEncodings() {
	c.forward(1);
	
	U16 encodingsCount = 0;
	c.readU16(encodingsCount);
	
	S32 * encodings = new S32[encodingsCount];
	
	for (U16 i=0; i<encodingsCount; i++) {
		c.readS32(encodings[i]);
	}
	
	filterEncodings(encodingsCount, &encodings);
	
	c.sendU16(encodingsCount);
//	c.send(encodings, sizeof(S32) * encodingsCount); // TODO -> pada proc ??
	for (U16 i=0; i<encodingsCount; i++) {
		c.sendS32(encodings[i]);
	}
	
	delete[] encodings;
}

void VncClientToServer::processEncodingType(int len) {
	for (int i=0; i<len; i++) {
		c.forward(4);
	}
}

void VncClientToServer::framebufferUpdateRequest() {
//	U8 incremental;
//	U16 xPosition, yPosition, width, height;
//	c.forwardU8(incremental);
//	c.forwardU16(xPosition);
//	c.forwardU16(yPosition);
//	c.forwardU16(width);
//	c.forwardU16(height);
	c.forward(9);
}

void VncClientToServer::keyEvent() {
	if (server.getConfig().isReplaying()) {
		c.read(7);
		return;
	}
	
	U8 downFlag;
	U32 key;
	
	c.forwardU8(downFlag);
	c.skip(2);
	c.forwardU32(key);
	
	if (server.getConfig().isRecording()) {
		server.getRecordWriter().recordKey(key, downFlag);
	}
}

void VncClientToServer::pointerEvent() {
	if (server.getConfig().isReplaying()) {
		c.read(5);
		return;
	}
	
	U8 buttonMask;
	U16 xPosition, yPosition;
	
	c.forwardU8(buttonMask);
	c.forwardU16(xPosition);
	c.forwardU16(yPosition);
	
	Screen & screen = server.getScreen();
	if (screen.isCursor()) screen.cursor->setPosition(xPosition, yPosition);
	
	if (server.getConfig().isRecording()) {
		server.getRecordWriter().recordPointer(buttonMask, xPosition, yPosition);
	}
}

void VncClientToServer::clientCutText() {
	c.read(3);
	
	U32 len;
	c.readU32(len);
	U8 * text = new U8[len];
	c.read(text, len);
	
	if (server.getConfig().isReplaying()) {
		delete[] text;
		return;
	}
	
	c.send(3);
	c.sendU32(len);
	c.send(text, len);
	
	if (server.getConfig().isRecording()) {
		server.getRecordWriter().recordCutText(len, text);
	}
	
	delete[] text;
}

