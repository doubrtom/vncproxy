#ifndef VNCCLIENTTOSERVER_H_876543456789
#define	VNCCLIENTTOSERVER_H_876543456789

#include "../common/Communicator.h"

namespace doubrtom {
	
	class Server;
	
	/**
	 * Class for process and resend VNC client to server messages.
	 */
	class VncClientToServer {
	public:
		
		enum Message {
			// basic
			SET_PIXEL_FORMAT = 0,
			SET_ENCODINGS = 2,
			FRAMEBUFFER_UPDATE_REQUEST = 3,
			KEY_EVENT = 4,
			POINTER_EVENT = 5,
			CLIENT_CUT_TEXT = 6,
			
			// other registered
			ANTHONY_LIGUORI = 255,
			VMWARE_1 = 254,
			VMWARE_2 = 127,
			GII = 253,
			TIGHT = 252,
			PIERRE_OSSMAN_SET_DESKTOP_SIZE = 251,
			COLIN_DEAN_XVP = 250,
			OLIVE_CALL_CONTROL = 249 
		};
		
		VncClientToServer(Server & s);
		~VncClientToServer();
		
		/**
		 * Set framebuffer encoding supported by proxy server for filter.
     */
		void setSupportedEncodings();
		/**
		 * Filter framebuffer encodings into encodings supportec by proxy server.
     * @param count
     * @param encodings
     */
		void filterEncodings(U16 & count, S32 ** encodings);
		
		/**
		 * Set socket of VNC client and server.
     * @param clientSocket		Socket for communication with client.
     * @param serverSocket		Socket for communication with server.
     */
		void setSockets(int clientSocket, int serverSocket);
		/**
		 * Proccess message from client.
     */
		void processMessage();
		
//		void resend(size_t len);
//		void resend(void * ptr, size_t len);
		
		/**
		 * Return true if msgType is blocking while replaying record, it is text, pointer and cutText msg.
     * @param msgType		Message type for checking
     * @return					True if block message, or false.
     */
		bool isBlockWhileReplaying(U8 msgType);
		
		// message method
		
		/**
		 * Method for processing 'SetPixelFormat' VNC message.
     */
		void setPixelFormat();
		/**
		 * Method for processing 'SetEncodings' VNC message.
     */
		void setEncodings();
		/**
		 * Method for processing resending specific framebuffer encoding.
     */
		void processEncodingType(int len);
		/**
		 * Message for processing 'FramebufferUpdateRequest' VNC message.
     */
		void framebufferUpdateRequest();
		/**
		 * Method for processing 'KeyEvent' VNC message.
     */
		void keyEvent();
		/**
		 * Method for processing 'PointerEvent' VNC message.
     */
		void pointerEvent();
		/**
		 * Method for processing 'ClientCutText' VNC message.
     */
		void clientCutText();
		
	protected:
		Server & server;
		Communicator c;
		
		static const int SUPPORTED_ENCODINGS_LEN = 6;
//		static const int SUPPORTED_ENCODINGS_LEN = 4;
		S32 supportedEncodings[SUPPORTED_ENCODINGS_LEN];
	};
	
}


#endif	/* VNCCLIENTTOSERVER_H */

