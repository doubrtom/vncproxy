#include <iostream>
#include <string.h>
#include <X11/Xmd.h>

#include "VncServerToClient.h"
#include "Server.h"
#include "../common/types.h"
#include "../common/Exception.h"
#include "../common/Rect.h"

#include "../screen/Screen.h"
#include "../common/ZlibDecoder.h"
#include "../screen/ScreenSaver.h"
#include "../screen/convertor/ConvertorTrueColour.h"
#include "../screen/convertor/ConvertorBase.h"
#include "../common/Cursor.h"

//#include "../screen/Screen.h"

using namespace std;
using namespace doubrtom;

VncServerToClient::VncServerToClient(Server & s) : server(s), c(s, "VncServerToClient") {
	
}

void VncServerToClient::setSockets(int clientSocket, int serverSocket) {
	c.setSockets(serverSocket, clientSocket);
}

void VncServerToClient::processMessage() {
	do {
		U8 msgType;
		c.forwardU8(msgType);

		switch (msgType) {
			case FRAMEBUFFER_UPDATE: {
				framebufferUpdate();
				if (server.getConfig().isScreenCapturing()) {
					server.getScreenCapture().addTask(new Screen(server.getScreen()));
				}
			}
				break;
			case SET_COLOUR_MAP_ENTRIES:
				setColourMapEntries();
				break;
			case BELL:
				bell();
				break;
			case SERVER_CUT_TEXT:
				serverCutText();
				break;
			default:
				throw Exception("Unsupported server to client message type - %d", (int) msgType);
		}

		c.flushBuffer();
	} while (!c.isEmptyIn());
}

void VncServerToClient::framebufferUpdate() {
	c.forward(1);
	
	U16 rectangleCount;
	c.forwardU16(rectangleCount);
	processUpdateRectangle(rectangleCount);
}

void VncServerToClient::processUpdateRectangle(int len) {
	for (int i=0; i<len; i++) {
		c.forwardU16(rect.xPosition);
		c.forwardU16(rect.yPosition);
		c.forwardU16(rect.width);
		c.forwardU16(rect.height);
		c.forwardS32(rect.encodingType);
		
		server.getConvertor()->setRect(rect.xPosition, rect.yPosition, rect.width, rect.height);
		
		processUpdateRectangleData();
	}
}

void VncServerToClient::processUpdateRectangleData() {
	switch (rect.encodingType) {
		case RAW:
			if (Server::DEV_MODE) printf("ENCODING: Raw !!!\n");
			rawEncoding();
			break;
		case COPY_RECT:
			if (Server::DEV_MODE) printf("ENCODING: Copy rect !!!\n");
			copyRectEncoding();
			break;
		case RRE:
			if (Server::DEV_MODE) printf("ENCODING: RRE !!!\n");
			rreEncoding();
			break;
		case HEXTILE:
			if (Server::DEV_MODE) printf("ENCODING: Hextile !!!\n");
			hextileEncoding();
			break;
		case ZRLE:
			if (Server::DEV_MODE) printf("ENCODING: ZRLE !!!\n");
			zrleEncoding();
			break;
		case CURSOR:
			cursorEncoding();
			break;
		case DESKTOP_SIZE:
			desktopSizeEncoding();
			break;
		default:
			throw Exception("Unsupported encoding type - %d.", (int)rect.encodingType);
			break;
	}
}

void VncServerToClient::setColourMapEntries() {
	c.forward(3);
	
	U16 coloursCount;
	c.forwardU16(coloursCount);
	
	ConvertorBase * convertoBase = server.getConvertor();
	ConvertorColourMap * convertor = dynamic_cast<ConvertorColourMap *>(convertoBase);
	if (convertor == NULL) throw Exception("ConvertorTrueColour not set before coulourMapEntries message.");
	
	convertor->createColourMap(coloursCount);
	
	processColourEntry(coloursCount, convertor);
}

void VncServerToClient::processColourEntry(U16 len, ConvertorColourMap * convertor) {
	U16 red, green, blue;
	
	for (unsigned int i=0; i<len; i++) {
		c.forwardU16(red);
		c.forwardU16(green);
		c.forwardU16(blue);
		
		convertor->insertColour(i, red, green, blue);
	}
}

void VncServerToClient::bell() {
	// do nothing, only message-type
}

void VncServerToClient::serverCutText() {
	c.forward(3);
	
	U32 len;
	c.forwardU32(len);
	U8 * text = new U8[len];
	
	c.forward(text, len);
	
	delete[] text;
}


/* ---------- ENCODING METHOD -------------- */

void VncServerToClient::rawEncoding() {
	size_t len = rect.width * rect.height * server.getBytesPerPixel();
	U8 * pixels = new U8[len];
	c.forward(pixels, len);
	
	server.getConvertor()->copyRect((void *) pixels);
	
	delete[] pixels;
}

void VncServerToClient::copyRectEncoding(){
	U16 srcX, srcY;
	c.forwardU16(srcX);
	c.forwardU16(srcY);
	
	server.getConvertor()->copyRect(srcX, srcY);
}

void VncServerToClient::rreEncoding() {
	U32 subrectangleCount;
	Pixel pixel;
	Rect rect;
	
	c.forwardU32(subrectangleCount);
	c.forwardPixel(pixel);
	
	server.getConvertor()->setPixels(pixel);
	
	U16 xPos, yPos, widht, height;
	for (U32 i=0; i<subrectangleCount; i++) {
		c.forwardPixel(pixel);
		c.forwardU16(xPos);
		c.forwardU16(yPos);
		c.forwardU16(widht);
		c.forwardU16(height);
		
		rect.set(this->rect.xPosition + xPos, this->rect.yPosition + yPos, widht, height);
		server.getConvertor()->setPixels(rect, pixel);
	}
}

void VncServerToClient::hextileEncoding() {
	U8 tileMask, subrectsCount, xyPos, widthHeight;
	Pixel bgPixel, fgPixel, colorPixel;
	Rect tile(rect.xPosition, rect.yPosition, 16, 16);
//	Rect tile(0, 0, 16, 16);
	Rect subrect;
	int bytes;
	int maxBytes = 16 * 16 * server.getBytesPerPixel();
	U8 * buff = new U8[maxBytes];
	
	for (int i=16; i<rect.height + 16; i+=16, tile.move(0, 16)) {
		if (i > rect.height) tile.height = rect.height - (i-16);
		else tile.height = 16;
		
		tile.x = rect.xPosition;
//		tile.x = 0;
		
		for (int j=16; j<rect.width + 16; j+=16, tile.move(16, 0)) {
			
			if (j > rect.width) tile.width = rect.width - (j-16);
			else tile.width = 16;
			
			c.forwardU8(tileMask);

			if (tileMask & HEXTILE_RAW) {
				bytes = tile.width * tile.height * server.getBytesPerPixel();
				c.forward(buff, bytes);
				
				server.getConvertor()->copyRect(tile, buff);
				
				continue;
			}
			
			if (tileMask & HEXTILE_BACKGROUND_SPECIFIED) {
				c.forwardPixel(bgPixel);
			}
			
			if (tileMask & HEXTILE_FOREGROUND_SPECIFIED) {
				c.forwardPixel(fgPixel);
			}
			
			server.getConvertor()->setPixels(tile, bgPixel);
			
			if (tileMask & HEXTILE_ANY_SUBRECTS) {
				c.forwardU8(subrectsCount);
				colorPixel = fgPixel;
				
				for (int k=0; k<subrectsCount; k++) {
					if (tileMask & HEXTILE_SUBRECTS_COLOURED) {
						c.forwardPixel(colorPixel);
					}
					
					c.forwardU8(xyPos);
					c.forwardU8(widthHeight);
					
					subrect.x = tile.x + ((xyPos >> 4) & 15);
					subrect.y = tile.y + (xyPos & 15);
					subrect.width = ((widthHeight >> 4) & 15) + 1;
					subrect.height = (widthHeight & 15) + 1;
					
					server.getConvertor()->setPixels(subrect, colorPixel);
				}
			}
			
		}
	}
	
	delete[] buff;
}

void VncServerToClient::zrleEncoding() {
//	U32 zlibDataLength = 0, dataLength = 0;
//	U8 * zlibData = NULL, * data = NULL;
//	
//	c.forwardU32(zlibDataLength);
//	if (!zlibDataLength) return;
//	
//	zlibData = new U8[zlibDataLength];
//	c.forward(zlibData, zlibDataLength);
//	
//	ZlibDecoder decoder;
//	decoder.decode(zlibData, zlibDataLength, data, dataLength);
	throw Exception("Not supported encoding.");
}

void VncServerToClient::cursorEncoding() {
	int cursorBytes = rect.width * rect.height * server.getBytesPerPixel();
	int bitmaskBytes = ((rect.width+7)/8) * rect.height;
	int bytes = cursorBytes + bitmaskBytes;
	
	if (bytes == 0) {
		server.getScreen().deleteCursor();
		
		return; // TODO co znamena zprava s width=0 a height=0 ???
	}
	
	U8 * buffer = new U8[cursorBytes];
	U8 * bitmask = new U8[bitmaskBytes];
	
	c.forward(buffer, cursorBytes);
	c.forward(bitmask, bitmaskBytes);
	
	server.getScreen().setCursor(new Cursor(buffer, bitmask, rect.width, rect.height, rect.xPosition, rect.yPosition, server));
	
	delete[] buffer;
	delete[] bitmask;
}

void VncServerToClient::desktopSizeEncoding() {
	server.getVncServerInfo().framebufferWidth = rect.width;
	server.getVncServerInfo().framebufferHeight = rect.height;
	
	server.initScreen();
}
