#ifndef VNCSERVERTOCLIENT_H_98765456789
#define	VNCSERVERTOCLIENT_H_98765456789

#include "../common/Communicator.h"
#include "../common/types.h"
#include "../common/vncInfo.h"

namespace doubrtom {
	
	class Server;
	class ConvertorColourMap;
	
	/**
	 * Class for process and resend VNC server to client messages.
	 */
	class VncServerToClient {
	public:
		
		enum Message {
			// basic
			FRAMEBUFFER_UPDATE = 0,
			SET_COLOUR_MAP_ENTRIES = 1,
			BELL = 2,
			SERVER_CUT_TEXT = 3,
			
			// other registered
			ANTHONY_LIGUORI = 255,
			VMWARE_1 = 254,
			VMWARE_2 = 127,
			GII = 253,
			TIGHT = 252,
			COLIN_DEAN_XVP = 250,
			OLIVE_CALL_CONTROL = 249 
		};
		
		enum EncodingType {
			RAW = 0,
			COPY_RECT = 1,
			RRE = 2,
			HEXTILE = 5,
			ZRLE = 16,
			CURSOR = -239, // pseudo-encoding
			DESKTOP_SIZE = -223,  // pseudo-encoding
		};
		
		enum HextileSubencodingMask {
			HEXTILE_RAW = 1,
			HEXTILE_BACKGROUND_SPECIFIED = 2,
			HEXTILE_FOREGROUND_SPECIFIED = 4,
			HEXTILE_ANY_SUBRECTS = 8,
			HEXTILE_SUBRECTS_COLOURED = 16
		};
		
		VncServerToClient(Server & s);
		
		/**
		 * Set socket of VNC client and server.
     * @param clientSocket		Socket for communication with client.
     * @param serverSocket		Socket for communication with server.
     */
		void setSockets(int clientSocket, int serverSocket);
		/**
		 * Proccess message from server.
     */
		void processMessage();
		
		// message method
		
		/**
		 * Method for processing 'FramebufferUpdate' VNC message.
     */
		void framebufferUpdate();
		/**
		 * Method for processing update rectangles from 'FramebufferUpdate' VNC message.
     */
		void processUpdateRectangle(int len);
		/**
		 * Method for processing update rectangles data from 'FramebufferUpdate' VNC message.
     */
		void processUpdateRectangleData();
		/**
		 * Method for processing 'SetColourMapEntries' VNC message.
     */
		void setColourMapEntries();
		/**
		 * Method for processing colour entry from VNC message 'SetColourMapEntries'.
     */
		void processColourEntry(U16 len, ConvertorColourMap * convertor);
		/**
		 * Method for processing 'Bell' VNC message.
     */
		void bell();
		/**
		 * Method for processing 'ServerCutText' VNC message.
     */
		void serverCutText();
		
		// encoding method
		
		/**
		 * Method for processing 'Raw' framebuffer encoding.
     */
		void rawEncoding();
		/**
		 * Method for processing 'CopyRect' framebuffer encoding.
     */
		void copyRectEncoding();
		/**
		 * Method for processing 'RRE' framebuffer encoding.
     */
		void rreEncoding();
		/**
		 * Method for processing 'Hextile' framebuffer encoding.
     */
		void hextileEncoding();
		/**
		 * Method for processing 'ZRLE' framebuffer encoding.
     */
		void zrleEncoding();
		/**
		 * Method for processing 'Cursor' framebuffer pseudo encoding.
     */
		void cursorEncoding();
		/**
		 * Method for processing 'DesktopSize' framebuffer pseudo encoding.
     */
		void desktopSizeEncoding();
		
		
	protected:
		Server & server;
		Communicator c;
		
		Rectangle rect;
	};
	
}


#endif	/* VNCSERVERTOCLIENT_H */

