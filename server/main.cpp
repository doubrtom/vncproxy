#include <cstdlib>
#include <iostream>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <error.h>

#include "Server.h"
#include "../common/Exception.h"
#include "ServerUnixSocketCConn.h"
#include "DConnection.h"

using namespace std;
using namespace doubrtom;

static int CONTROL = 0;
static int VNC = 1;

Server server;

struct Arg {

	Arg(int t, Server & s) : type(t), server(s) {
	}

	int type;
	Server & server;
};

/**
 * Function for control thread.
 * @param arg	Argument with data for thread.
 */
void controlThread(Arg * arg) {
	try {
    server.getLogger().log("Control thread created.");
		ServerUnixSocketCConn con(arg->server);
		con.createSocket();
		con.processControlCommunication();
	} catch (Exception & ex) {
		cerr << ex.getMessage() << endl;
		server.stopServer();
		return;
	}
}

/**
 * Function for main (work) thread.
 * @param arg	Argument with data for thread.
 */
void vncThread(Arg * arg) {
	while (server.isRunning()) {
		try {
      server.getLogger().log("VNC communication created.");
			DConnection con(arg->server);
			con.startVncCommunication();
		} catch (Exception & ex) {
			cerr << ex.getMessage() << endl;
			
			
			if ( ex.getCode() == Exception::EX_DCONN_COMM 
				|| ex.getCode() == Exception::EX_COMM_CLOSED_BY_PEER
        || ex.getCode() == Exception::EX_VNC_HAND_SHAKING)
			{
        server.getLogger().log("VNC communication close.\n");
				server.reinit();
				continue; 
			}
			
      server.getLogger().log("VNC proxy server end: %s.\n", ex.getMessage());
			server.stopServer();
			return;
		}
		
		return;
	}
}

/**
 * Function for create new thread.
 * @param arg		Argument with data for thread.
 */
void createThread(Arg * arg) {
	if (arg->type == CONTROL) {
		controlThread(arg);
	} else {
		vncThread(arg);
	}
}

/**
 * System signal handler.
 * @param signum	Number of signal.
 */
static void signalHandler(int signum) {
	if (signum == SIGTERM) {
		server.stopServer();
		signal(SIGTERM, SIG_DFL);
	} else if (signum == SIGINT) {
		server.stopServer();
		signal(SIGINT, SIG_DFL);
	}
}

/**
 * Print usage of vncproxyserver, if missing mandatory parameters.
 * @param argv		Arguments of program.
 */
void printUsage(char ** argv) {
	cout << "usage: ";
	cout << argv[0] << " :display <OPTIONS>" << endl;
	cout << "example: " << argv[0] << " :2 -server localhost:1" << endl;
}

/**
 * Check if thread was created sucessfully.
 * @param threadId		Thread id.
 */
void checkThread(int threadId) {
	if (threadId == 0) return;
	
	cerr << "Cannot create new thread: " << strerror(threadId) << endl;
	exit(1);
}

/*
 * Main function of VNC proxy server.
 */
int main(int argc, char** argv) {

	if (argc == 1) {
		printUsage(argv);
		return 0;
	}

	// register signals
	signal(SIGHUP, signalHandler); // TODO odchytavat na restart nebo zbytecne ?
	signal(SIGTERM, signalHandler);
	signal(SIGINT, signalHandler);
	
	Config & config = server.getConfig();

	try {
		config.createProgramDir();
		config.readConfigFile();
		config.processParameters(argc, argv);
		config.checkConfig();
	} catch (Exception & ex) {
		cerr << ex.getMessage() << endl;
		return 1;
	}
  
  // init logger
  server.getLogger().init(server.getConfig().getHost(), server.getConfig().getProxyDisplay());
  server.getLogger().log("Start VNC proxy server.\n\n");
	
	pthread_t controlThread;

	Arg argControl(CONTROL, server);
	Arg argServer(VNC, server);

	int controlThreadId = pthread_create(&controlThread, NULL, (void * (*)(void *)) createThread, (void *) &argControl);
	checkThread(controlThreadId);
	
	vncThread(&argServer);

	pthread_join(controlThread, NULL);
  
	return 0;
}
